# VUE



#### 1.vue初体验

npm i @vue2.6.10

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="app">
    <h2>{{message}}</h2>
    <h1>{{name}}</h1>
</div>

<div>{{message}}</div>

<script src="../js/vue.js"></script>
<script>
    // let(变量)/const(常量)
    // 编程范式: 声明式编程
    const app = new Vue({
        el: '#app', // 用于挂载要管理的元素
        data: { // 定义数据
            message: '你好啊,李焕英!',
            name: 'coderwhy'
        }
    })
</script>

</body>
</html>
```

**如果要是用原生的js如何实现在document上面出现我们想要出现的数据呢？**

  元素js的做法(编程范式: 命令式编程)

   1.创建div元素,设置id属性

   2.定义一个变量叫message

   3.将message变量放在前面的div元素中显示

   4.修改message的数据: 今天天气不错!

   5.将修改后的数据再次替换到div元素

#### 2.课堂小案例 计数器

现在，我们来实现一个小的计数器
点击 + 计数器+1
点击 -  计数器 -1

这里，我们又要使用新的指令和属性了
新的属性：methods，该属性用于在Vue对象中定义方法。
新的指令：@click, 该指令用于监听某个元素的点击事件，并且需要指定当发生点击时，执行的方法(方法通常是methods中定义的方法)  @符号是v-on的简写

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="app">
    <h2>当前计数: {{counter}}</h2>
    <button v-on:click="add">+</button>
    <button v-on:click="sub">-</button>
</div>
<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            counter:0
        },
        methods: {
            add: function () {
                console.log('add被执行');
                this.counter++
            },
            sub: function () {
                console.log('sub被执行');
                this.counter--
            }
        },
    })
</script>
</body>
</html>
```

#### 什么是MVVM

MVVM是 Model-View-ViewModel 的缩写，它是一种软件架构风格

​	Model：模型， 数据对象（data选项当中 的）

​	View：视图，模板页面（用于渲染数据） 

​	ViewModel：视图模型，其实本质上就是 Vue 实例

它的哲学思想是：

通过数据驱动视图

把需要改变视图的数据初始化到 Vue中，然后再通过修改 Vue 中的数据，从而实现对视图的更新。

声明式编程：按照 Vue 的特定语法进行声明开发，就可以实现对应功能，不需要我们直接操作Dom元素

命令式编程：Jquery它就是，需要手动去操作Dom才能实现对应功能。

![1617423974597](./media/1617423974597.png)

##### 关于三个图层之间的关系

![1617424363145](./media/1617424363145.png)

##### 小案例计数器里面的mvvm

![1617424666216](./media/1617424666216.png)

#### 小知识

**el** ：里面不光可以传入 string类型的字符串"#app" 

还可以传入一个自己在前面定义的html标签 eg：document.querySelector("#id"),

**data**：里面传入对象和函数  但是 在组件之中只能传入函数

Vue实例对应的数据对象

##### 开发之中什么是函数 什么是方法？

方法：method

函数：function

简单来说 方法基本与实例对象是挂钩的  而函数则是在外面i定义的

​      方法有主 函数无主   

# VUE的生命周期

每个 Vue 实例在被创建时都要经过一系列的初始化过程

<img src="D:/BUKA/VUE/day50vue/笔记/media/wps3.png">生命周期分为三大阶段：初始化显示、更新显示、销毁Vue实例

​	<img src="D:/BUKA/VUE/day50vue/笔记/media/wps4.png">初始化阶段的钩子函数：

​		beforeCreate() 实例创建前：数据和模板均未获取到

​		**created() 实例创建后: 最早可访问到 data 数据，但模板未获取到（一般发送AJAX请求前 放到这个函数中）**

​		beforeMount() 数据挂载前：模板已获取到，但是数据未挂载到模板上。

​		**mounted() 数据挂载后： 数据已挂载到模板中**

​	<img src="D:/BUKA/VUE/day50vue/笔记/media/wps5.png">更新阶段的钩子函数：

​		beforeUpdate() 模板更新前：data 改变后，更新数据模板前调用

​		updated() 模板更新后：将 data 渲染到数据模板中

<img src="D:/BUKA/VUE/day50vue/笔记/media/wps6.png">销毁阶段的钩子函数：

​		**beforeDestroy() 实例销毁前（一般用来做一些收尾工作）**

​		destroyed() 实例销毁后

 

![1617426019099](./media/1617426019099.png)

![1617426030320](./media/1617426030320.png)

代码演示

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <h1>{{msg}}</h1>
    </div>
    <script src="./node_modules/vue/dist/vue.js"></script>
    <script>
         const vm = new Vue({
            // el:"#app",  
            data:{
                msg:"那就这样吧"
            },
            beforeCreate() {
                //vue 实例创建前被调用，数据和模板均未获取到
                //应用的场景相对少，因为这个钩子函数阶段什么都还没获取到
                console.log("beforeCreate",this.$el,this.$data);
            },
            created() {
                //vue实例创建后，最早可以获取到data数据的钩子，但是模板未获取到
                //也就是说在这个阶段能够获取到数据，但是并没有渲染到模板中，因为模板还没有获取到
                //建议在这里发送ajax异步请求
                console.log("created",this.$el,this.$data);
            },
            beforeMount() {
                //数据挂载之前，获取到了模板，但是数据还未挂载到模板上
                console.log("beforeMount",this.$el,this.$data);
            },
            mounted() {
                //数据已经挂载到模板中了
                console.log("mounted",this.$el,this.$data);
            },
            beforeUpdate() {
                //想要测试数据更新，可以在浏览器console栏中         输入vm.message="xxx"进行测试
            //当data数据更新之后，去更新模板中的数据前调用
                console.log("beforeUpdate",this.$el.innerHTML,this.$data);
            },
            updated() {
                //更新完成之后
                console.log("updated",this.$el.innerHTML,this.$data);
                
            },
            beforeDestroy() {
                //销毁vue实例之前调用
                //收尾工作，回收，释放之类的，比较常用
                console.log("beforeDestroy");
            },
            destroyed() {
                //销毁vue实例之后调用
                console.log("destroyed");
            },
        }).$mount("#app")//如果实例中没有el选项，可使用$mount()手动挂载Dom

// vm.$destroy();//销毁vue实例方法
    </script>
</body>
</html>
```

输出结果

![1617427468474](./media/1617427468474.png)

![1617427493616](./media/1617427493616.png)

#### 什么是Mustache语法

如何将data中的文本数据，插入到HTML中呢？
 我们已经学习过了，可以通过Mustache语法(也就是双大括号)。
英文翻译：Mustache: 胡子/胡须. （ma si ta chi） 
我们可以像下面这样来使用，并且数据是响应式的

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<div id="app">
  <h2>{{message}}</h2>
  <h2>{{message}}, 李银河!</h2>

  <!--mustache语法中,不仅仅可以直接写变量,也可以写简单的表达式-->
  <h2>{{firstName + lastName}}</h2>
  <h2>{{firstName + ' ' + lastName}}</h2>
  <h2>{{firstName}} {{lastName}}</h2>
  <h2>{{counter * 2}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      firstName: 'kobe',
      lastName: 'bryant',
      counter: 100
    },
  })
</script>
</body>
</html>
```

运行成果

![1617428372849](./media/1617428372849.png)

#### VUE基本插值指令

##### 1.v-once

只希望出现一次 如果model数据层改变的话 v-once所在的标签不会发生改变

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <h2>{{message}}</h2>
  <h2 v-once>{{message}}</h2>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>
</body>
</html>
```

![1617428696741](./media/1617428696741.png)

通过vue.js-devtools插件修改数据后 下面v-once的h2标签并未发生改变

v-once后面不用添加任何表达式

##### 2.v-html

将html标签内容也能通过数据层显示到DOM之中

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <h2>{{url}}</h2>
    <!-- 以html格式返回我们所要的数据-->
  <h2 v-html="url"></h2>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      url: '<a href="http://www.baidu.com">百度一下</a>'
    }
  })
</script>
</body>
</html>
```

显示结果

![1617433606730](./media/1617433606730.png)

##### 3.v-text

实际作用就是把在数据层的数据拿出来返回到视图之中 

但是我们大多数时候不会使用这种方法 因为 不够灵活,**如果我们通过Mustache方法来获取值的时候 会进行拼接 而用v-text这种方法的时候会进行覆盖**

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <h2>{{message}}, 王耀达!</h2>
  <h2 v-text="message">, 王耀达!</h2>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>
</body>
</html>
```

示例

![1617434184275](./media/1617434184275.png)

##### 4.v-pre

不解析我们Mustache语法中的内容 让里面的东西原封不动的解析出来 就采用这个标签

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <h2>{{message}}</h2>
  <h2 v-pre>{{message}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>
</body>
</html>
```

示例

![1617434419923](./media/1617434419923.png)

##### 5.v-cloak(很少使用)

在某些情况下，我们浏览器可能会直接显然出未编译的Mustache标签。
cloak: 斗篷

【因为浏览器是从上往下执行的 所以在我们解析到dom元素的时候 他自己会解析出来 我们定义的 vue入口 所以我们使用v-cloak来防止直接解析出dom中所定义的入口】

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
  <style>
    [v-cloak] {
      display: none;
    }
  </style>
</head>
<body>
<div id="app" v-cloak>
  <h2>{{message}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  // 在vue解析之前, div中有一个属性v-cloak
  // 在vue解析之后, div中没有一个属性v-cloak 就会自动去除上面CSS里面的内容
  setTimeout(function () {
    const app = new Vue({
      el: '#app',
      data: {
        message: '你好啊'
      }
    })
  }, 1000)
</script>
</body>
</html>
```

#### V-bind

**动态绑定某一个属性**

前面我们学习的指令主要作用是将值插入到我们模板的内容当中。
但是，除了内容需要动态来决定外，某些属性我们也希望动态来绑定。
比如动态绑定a元素的href属性
比如动态绑定img元素的src属性

这个时候，我们可以使用v-bind指令：

**作用：动态绑定属性**
**缩写：:**
**预期：any (with argument) | Object (without argument)**
**参数：attrOrProp (optional)**

***\*v-bind中几种绑定style\**** ***\*和class的方式\****

注意点

1. 如果需要通过v-bind给class绑定类名, 那么不能直接赋值
   默认情况下v-bind会去Model中查找数据, 但是Model中没有对应的类名, 所以无效, 所以不能直接赋值

##### 1.什么是V-bind中的语法糖呢？

就是v-bind的简写方式 就是V-bind的语法糖（语法糖的出现和使用会更方便 ,也使得代码更加简洁）

##### 2.v-bind绑定class

绑定class分两种方式

1.对象语法              2.数组语法

###### 类绑定对象语法的具体使用

用法一：直接通过{}绑定一个类
```html
<h2 :class="{'active': isActive}">Hello World</h2>
```

用法二：也可以通过判断，传入多个值
```vue
<h2 :class="{'active': isActive, 'line': isLine}">Hello World</h2>
```

用法三：和普通的类同时存在，并不冲突
注：如果isActive和isLine都为true，那么会有title/active/line三个类

```vue
<h2 class="title" :class="{'active': isActive, 'line': isLine}">Hello World</h2>
```

用法四：如果过于复杂，可以放在一个methods或者computed中
注：classes是一个计算属性

```vue
<h2 class="title" :class="classes">Hello World</h2>
```

源码使用

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>

  <style>
    .active {
      color: red;
    }
  </style>
</head>
<body>

<div id="app">
  <!--<h2 class="active">{{message}}</h2>-->
  <!--<h2 :class="active">{{message}}</h2>-->
  <!--<h2 v-bind:class="{key1: value1, key2: value2}">{{message}}</h2>-->
  <!--<h2 v-bind:class="{类名1: true, 类名2: boolean}">{{message}}</h2>-->
  <h2 class="title" v-bind:class="{active: isActive, line: isLine}">{{message}}</h2>
  <h2 class="title" v-bind:class="getClasses()">{{message}}</h2>
  <button v-on:click="btnClick">按钮</button>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      isActive: true,
      isLine: true
    },
    methods: {
      btnClick: function () {
        this.isActive = !this.isActive
      },
      getClasses: function () {
        return {active: this.isActive, line: this.isLine}
      }
    }
  })
</script>

</body>
</html>
```

###### 类绑定数组语法的具体使用(使用次数很少)

【类的特别多的时候 可以用这种数组方法 但是在使用中比较少】

用法一：直接通过[]绑定一个类

```vue
<h2 :class="['active']">Hello World</h2>
```


用法二：也可以传入多个值

```vue
<h2 :class=“[‘active’, 'line']">Hello World</h2>
```

用法三：和普通的类同时存在，并不冲突
注：会有title/active/line三个类

```vue
<h2 class="title" :class=“[‘active’, 'line']">Hello World</h2>
```

用法四：如果过于复杂，可以放在一个methods或者computed中
注：classes是一个计算属性

```vue
<h2 class="title" :class="classes">Hello World</h2>
```

##### 3.v-bind绑定style

###### 对象语法绑定style

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
  <style>
    .title {
      font-size: 50px;
      color: red;
    }
  </style>
</head>
<body>

<div id="app">
    key是css的一个属性
  <!--<h2 :style="{key(属性名): value(属性值)}">{{message}}</h2>-->

  <!--'50px'必须加上单引号, 否则是当做一个变量去解析-->
  <!--<h2 :style="{fontSize: '50px'}">{{message}}</h2>-->

  <!--finalSize当成一个变量使用-->
  <!--<h2 :style="{fontSize: finalSize}">{{message}}</h2>-->
  <h2 :style="{fontSize: finalSize + 'px', backgroundColor: finalColor}">{{message}}</h2>
  <h2 :style="getStyles()">{{message}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      finalSize: 100,
      finalColor: 'red',
    },
    methods: {
      getStyles: function () {
        return {fontSize: this.finalSize + 'px', backgroundColor: this.finalColor}
      }
    }
  })
</script>

</body>
</html>
```

###### 数组语法绑定style

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<div id="app">
  <h2 :style="[baseStyle, baseStyle1]">{{message}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      baseStyle: {backgroundColor: 'red'},
      baseStyle1: {fontSize: '100px'},
    }
  })
</script>

</body>
</html>
```

#### 计算属性

在计算属性中 尽量少使用动词起名字

计算属性里面是有缓存的

**计算属性的使用为什么不使用后面的括号呢？**

因为我们常常使用计算属性的时候 我们很少 甚至没有使用他的set属性，也就是从某种意义上来说，我们不需要往里面传入参数,不需要我们设置值，所以我们就不会使用后面的括号

```html
 <!--在函数中使用的话 需要加上括号-->
<h2>{{getFullName()}}</h2>
  <!-- 计算属性在使用的时候可以不用添加括号 -->
  <h2>{{fullName}}</h2>
```



##### 1.计算属性的使用

```vue
<div id="app">
  <h2>{{firstName + ' ' + lastName}}</h2>
  <h2>{{firstName}} {{lastName}}</h2>

  <h2>{{getFullName()}}</h2>
  <!-- 计算属性在使用的时候可以不用添加括号 -->
  <h2>{{fullName}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      firstName: 'Lebron',
      lastName: 'James'
    },
    // computed: 计算属性()
    computed: {
      fullName: function () {
        return this.firstName + ' ' + this.lastName
      }
    },
    methods: {
      getFullName() {
        return this.firstName + ' ' + this.lastName
      }
    }
  })
</script>

</body>
</html>
```

##### 2.计算属性的setter和getter

计算属性中一般包含两种方法 

一个是set方法  一个是get方法  
【一般计算属性中没有set方法,所以大多数情况下 我们只使用get方法（因为计算属性 我们只需要他返回的值）也就是只是读取属性，】

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <h2>{{fullName}}</h2>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      firstName: 'Kobe',
      lastName: 'Bryant'
    },
    computed: {
      // 计算属性一般是没有set方法, 只读属性.
      fullName: {
        set: function(newValue) {
          console.log('-----', newValue);
        },
        get: function () {
          return this.firstName + ' ' + this.lastName
        }
      },
    }
  })
</script>
</body>
</html>
```

示例

![1617544692997](./media/1617544692997.png)

**计算属性完整版的使用**

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <h2>{{fullName}}</h2>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      firstName: 'Kobe',
      lastName: 'Bryant'
    },
    computed: {
      // 计算属性一般是没有set方法, 只读属性.
      fullName: {
        set: function(newValue) {
          // console.log('-----', newValue);
          const names = newValue.split(' ');
          this.firstName = names[0];
          this.lastName = names[1];
        },
        get: function () {
          return this.firstName + ' ' + this.lastName
        }
      },
    }
  })
</script>
</body>
</html>
```

split 以（" "）空格分开【在这种计算属性中 set方法得以应用 输出结果我们看下图】

![1617545150431](./media/1617545150431.png)

可以看到 document上面的内容 和我们之前获取属性的不一样了，

所以设置属性得到了应用

##### 3.计算属性和methhods的区别

计算属性中是有缓存的部分 当我们需要多次调用的时候 我们需要选择计算属性

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <!--1.直接拼接: 语法过于繁琐-->
  <h2>{{firstName}} {{lastName}}</h2>
  <!--2.通过定义methods-->
  <h2>{{getFullName()}}</h2>
  <h2>{{getFullName()}}</h2>
  <h2>{{getFullName()}}</h2>
  <h2>{{getFullName()}}</h2>
  <!--3.通过computed-->
  <h2>{{fullName}}</h2>
  <h2>{{fullName}}</h2>
  <h2>{{fullName}}</h2>
  <h2>{{fullName}}</h2>
</div>
<script src="../js/vue.js"></script>
<script>
  // angular -> google
  // TypeScript(microsoft) -> ts(类型检测)
  // flow(facebook) ->
  const app = new Vue({
    el: '#app',
    data: {
      firstName: 'Kobe',
      lastName: 'Bryant'
    },
    methods: {
      getFullName: function () {
        console.log('getFullName');
        return this.firstName + ' ' + this.lastName
      }
    },
    computed: {
      fullName: function () {
        console.log('fullName');
        return this.firstName + ' ' + this.lastName
      }
    }
  })
</script>
</body>
</html>
```

例如上述代码getFullName()和FullName各自调用了四次 但是在我们在下面打印出调用情况

![1617589295601](./media/1617589295601.png)

可以看到 计算属性中的fullName只是调用了一次 而getFullName调用了四次  所以证明到 计算属性是有缓存的 

【当我们进行大规模的逻辑运算的时候 ，我们不需要把逻辑运算放在HTML之中,而是放在methods和computed之中，如果多次调用且值不变的情况下 使用计算属性,如果需要值改变则需要使用函数】

#### V-on

事件监听

作用：绑定事件监听器
缩写：@

##### v-on的基本用法

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<div id="app">
  <h2>{{counter}}</h2>
  <button @click="increment">+</button>
  <button @click="decrement">-</button>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      counter: 0
    },
    methods: {
      increment() {
        this.counter++
      },
      decrement() {
        this.counter--
      }
    }
  })
</script>
</body>
</html>
```

##### v-on参数传递问题

**1.事件调用的方法没有参数**

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<div id="app">
  <!--1.事件调用的方法没有参数-->
  <button @click="btn1Click()">按钮1</button>
  <button @click="btn1Click">按钮1</button>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      abc: 123
    },
    methods: {
      btn1Click() {
        console.log("btn1Click");
      }
    }
  })
</script>
</body>
</html>
```

输出结果

![1617616566665](./media/1617616566665.png)

**2.在事件定义时, 写函数方法时省略了小括号, 但是方法本身是需要一个参数的, 这个时候,Vue会默认将浏览器生产的event事件对象作为参数传入到方法**

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <button @click="btn2Click(123)">按钮2</button>
  <button @click="btn2Click()">按钮2</button>
  <button @click="btn2Click">按钮2</button>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      abc: 123
    },
    methods: { 
      btn2Click(event) {
        console.log('--------', event);
      },
    }
  })
</script>
</body>
</html>
```

输出结果

![1617616791506](./media/1617616791506.png)

解析：

【第一个按钮:因为写了小括号并且往里面传入参数并且在下面methods里面要求指定传入参数了 所以输出-----123】

【第二个按钮:写了小括号 但是没往里面传入参数，所以没找到传入的参数，便是undefind】

【第三个按钮:没写小括号，但是我们方法定义需要传入参数,vue自己给我们返回一个事件对象作为参数传入到方法里面了】

**3.方法定义时, 我们需要event对象, 同时又需要其他参数 在调用方式, 如何手动的获取到浏览器参数的event对象: $event**

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <button @click="btn3Click(abc, $event)">按钮3</button>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      abc: 123
    },
    methods: {
      btn1Click() {
        console.log("btn1Click");
      },
      btn2Click(event) {
        console.log('--------', event);
      },
      btn3Click(abc, event) {
        console.log('++++++++', abc, event);
      }
    }
  })
</script>
</body>
</html>
```

输出结果

![1617617262455](./media/1617617262455.png)

解析

【按钮3：我们定义方法需要传入参数，并且还需要传入事件对象，所以我们要在调用的时候手动获取浏览器参数的event对象 ：$event】

##### v-on修饰符

.stop阻止单击事件继续传播

.prevent阻止事件默认行为

.once点击事件将只会触发一次

```vue
<body>	

<div id="app">

<h2>1. 事件处理方法</h2>

<button @click="say">Say {{msg}}</button>

<button @click="warn('hello', $event)">Warn</button>

<br>

<h2>2. 事件修饰符</h2>

<!--单击事件继续传播-->

<div @click="todo">

<!--点击后会调用doThis再调用todo-->

<button @click="doThis">单击事件会继续传播</button>

</div>

<br/>

<!-- 阻止单击事件继续传播，-->

<div @click="todo">

<!--点击后只调用doThis-->

<button @click.stop="doThis">阻止单击事件会继续传播</button>

</div>

<!-- 阻止事件默认行为 -->

<a href="http://www.ceshi.com" @click.prevent="doStop">测试官网</a>

 <!-- 点击事件将只会触发一次 -->
 <button @click.once="doOnly">点击事件将只会触发一次: {{num}}</button>
 </div> 
 <script src="./node_modules/vue/dist/vue.js"></script>
 <script type="text/javascript">
 var vm = new Vue({
 el: '#app',
 data: {
 msg: 'Hello, Vue.js',
 num: 1
	},
 methods: {
 say: function (event) {
 // `this` 在方法里指向当前 Vue 实例
 alert(this.msg)
 // `event` 是原生 DOM 事件
 alert(event.target.innerHTML)
	},
 //多个参数如果需要使用原生事件，将 $event 作为实参传入
 warn: function (msg, event) {
 alert(msg + "," + event.target.tagName)
	},
 todo: function () {
 alert("todo");
	},
 doThis: function () {
 alert("doThis");
	},
 doStop: function () {
 alert("href默认跳转被阻止	")
	},
 doOnly: function() {
 this.num++
	}
	}
	})
 </script>
 </body>
```

1. 什么时候元素被渲染

   v-if)如果在初始条件为假，则什么也不做，每当条件为真时，都会重新渲染条件元素    v-show不管初始条件是什么，元素总是会被渲染，并且只是简单地基于 CSS 进行切换

2. 使用场景选择

   v-if有更高的切换开销，

   v-show有更高的初始渲染开销。

**因此，如果需要非常频繁地切换，则使用 v-show[display不断地切换] 较好；如果在运行后条件很少改变，则使用 v-if [创建一个元素]较好。**

#### V-if,V-else-if,V-else

##### V-if v-else使用

```vue
<body>
<div id="app">
  <button @click="butclick">取反按钮</button>
  <h2 v-if="isShow">
    <div>abc</div>
    <div>abc</div>
    <div>abc</div>
    <div>abc</div>
    <div>abc</div>
    {{message}}
  </h2>
  <h1 v-else>isShow为false时, 显示我</h1>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      isShow: true
    },
    methods:{
     butclick(){
      this.isShow =!this.isShow
     }
    }
  })
</script>
</body>
```

显示结果

![1617880933180](./media/1617880933180.png)

![1617880940243](./media/1617880940243.png)

按下取反按钮后显示v-else的内容 

**什么是虚拟dom我们下面写一个小案例来表述虚拟DOM**

登录用户小案例

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <span v-if="isUser">
    <label for="username">用户账号</label>
    <input type="text" id="username" placeholder="用户账号">
  </span>
  <span v-else>
    <label for="email">用户邮箱</label>
    <input type="text" id="email" placeholder="用户邮箱">
  </span>
  <button @click="isUser = !isUser">切换类型</button>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      isUser: true
    }
  })
</script>
</body>
</html>
```

示例结果

点击切换类型 切换前面的账号和邮箱和后面的input页面

# ![1617884352244](./media/1617884352244.png)

![1617884359486](./media/1617884359486.png)

问题：我们点击切换类型按钮  切换用户账号，用户邮箱，和后面的input，但是我们切换了 。本来在用户账号输入的内容 切换后为什么input里面还是我们之前输入的内容。

为什么会出现这个问题？

问题解答：

这是因为Vue在进行DOM渲染时，出于性能考虑，会尽可能的复用已经存在的元素，而不是重新创建新的元素。
在上面的案例中，Vue内部会发现原来的input元素不再使用，直接作为else中的input来使用了。

解决方案：

如果我们不希望Vue出现类似重复利用的问题，可以给对应的input添加key
并且我们需要保证key的不同【加上key之后就解决问题了，会自己对比两个key是否一样,如果不一样不会在进行复用】

解决代码

```vue
<body>
<div id="app">
  <span v-if="isUser">
    <label for="username">用户账号</label>
    <input type="text" id="username" placeholder="用户账号" key="username">
  </span>
  <span v-else>
    <label for="email">用户邮箱</label>
    <input type="text" id="email" placeholder="用户邮箱" key="email">
  </span>
  <button @click="isUser = !isUser">切换类型</button>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      isUser: true
    }
  })
</script>
</body>
```

#### V-For

v-for的使用

##### 遍历数组

```vue
<body>
<div id="app">
  <!--1.在遍历的过程中,没有使用索引值(下标值)-->
  <ul>
    <li v-for="item in names">{{item}}</li>
  </ul>

  <!--2.在遍历的过程中, 获取索引值-->
  <ul>
    <li v-for="(item, index) in names">
      {{index+1}}.{{item}}
    </li>
  </ul>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      names: ['wyd', 'kobe', 'james', 'curry']
    }
  })
</script>
</body>
```

![1617958311133](./media/1617958311133.png)

##### 遍历对象

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<div id="app">
  <!--1.在遍历对象的过程中, 如果只是获取一个值, 那么获取到的是value-->
  <ul>
    <li v-for="item in info">{{item}}</li>
  </ul>

  <!--2.获取key和value 格式: (value, key) -->
  <ul>
    <li v-for="(value, key) in info">{{value}}-{{key}}</li>
  </ul>

  <!--3.获取key和value和index 格式: (value, key, index) -->
  <ul>
    <li v-for="(value, key, index) in info">{{value}}-{{key}}-{{index}}</li>
  </ul>
  
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      info: {
        name: 'why',
        age: 18,
        height: 1.88
      }
    }
  })
</script>
</body>
</html>
```

输出结果

![1617958768452](./media/1617958768452.png)

##### V-for绑定key和不绑定key的区别

官方推荐我们在使用v-for时，给对应的元素或组件添加上一个:key属性。vue数据发生改变后，界面也会重新渲染

#### splice

**splice（）删除元素 也可与往里面传入参数** 

**eg：splice（2，2）**

**【第一个2是从第几个位置开始删除,后面那个2是删除几个元素】**

**eg：splice（2，0，"F"）**

**【第一个2是从第几个位置开始删除,第二个数是删除0个元素，最后一个'F'是在第二个元素的后面添加一个F】**

示例

![1617965283739](./media/1617965283739.png)

**绑定的key尽量和我们所遍历的数组对应【就比如说不对应index，因为index是索引他不和我们所遍历的数组对应】**

我们加的是:key是item也不是index

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>  
<div id="app">
  <ul>
    <li v-for="item in letters" :key="item">{{item}}</li>
  </ul>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      letters: ['A', 'B', 'C', 'D', 'E']
    }
  })
</script>

</body>
</html>
```

**什么是DIff算法？ **diff【different】

如果我们要在B和C之中要插入一个字母F这样我们第一时间传入到虚拟dom之中，去对比原来DOM之中缺少的元素 从而写入我们要传入的DOM之中

**:key绑定的重要性**

![1618031606379](./media/1618031606379.png)

B和C之间要插入一个F的话 以我们的理解   直接在 B和C之间插入一个F就可以，直接插入li6这个编号 显示F 再通过DIFF算法对比dom有什么不同,就可以显示出来我们后面插入的元素，但是我们通过VUE底层来做这个事情呢 ，他的做法和我们的不是一样的，他是先让原本位置为3的C变成F，然后在依次往下排序，4->C  5->D  6->E  这样性能就低下了 对比到DOM也是不同，所以我们要加上一个KEY来提高性能【因为有了key的话，先将key和我们后加入·的数据进行对照】

![1618031817597](./media/1618031817597.png)

#### 数据是响应式的

##### 什么是响应式？

简单来说就是数组（数据）发生变化，界面能自动发生刷新；

##### 数组中有哪些方法是响应式的

```javascript
// 1.push方法
        // this.letters.push('aaa')
        // this.letters.push('aaaa', 'bbbb', 'cccc')

        // 2.pop(): 删除数组中的最后一个元素
        // this.letters.pop();

        // 3.shift(): 删除数组中的第一个元素
        // this.letters.shift();

        // 4.unshift(): 在数组最前面添加元素
        // this.letters.unshift()
        // this.letters.unshift('aaa', 'bbb', 'ccc')

        // 5.splice作用: 删除元素/插入元素/替换元素
        // 删除元素: 第二个参数传入你要删除几个元素(如果没有传,就删除后面所有的元素)
        // 替换元素: 第二个参数, 表示我们要替换几个元素, 后面是用于替换前面的元素
        // 插入元素: 第二个参数, 传入0, 并且后面跟上要插入的元素
        // splice(start)
        // splice(start):
        // this.letters.splice(1, 3, 'm', 'n', 'l', 'x')
        // this.letters.splice(1, 0, 'x', 'y', 'z')

        // 5.sort()  //排序方法
        // this.letters.sort()

        // 6.reverse()
        // this.letters.reverse()
```

#### 过滤器

##### **1.什么叫做过滤器？**

过滤器和函数和计算属性一样 都是用来处理数据的，但是过滤器常常用来格式化插入的文本数据

##### **2.如何自定义一个过滤器？**

详细过程参观 vue.js中文文档

Vue.filter(“过滤器名称”,过滤器处理函数)：

Eg：
如何自定义一个全局过滤器
通过Vue.filter();
filter方法接收两个参数
第一个参数: 过滤器名称
第二个参数: 处理数据的函数
注意点: 默认情况下处理数据的函数接收一个参数,就是当前要被处理的数据

```javascript
Vue.filter("formartStr1", function (value) {
  // console.log(value); 先输出看看 是不是我们要拿到的数据
  value = value.replace(/学院/g, "大学"); replace 替换 g代表全局替换
  console.log(value); 确认数据修改完毕
  //必须返回你要处理的事件当中
  return value;
});
```

##### 3.如何使用过滤器

```javascript
{{msg | 过滤器名称}}
:value="msg | 过滤器名称"
eg：
<div id="app">
    <!--Vue会把name交给指定的过滤器处理之后, 再把处理之后的结果插                      到指定的元素中-->
    <p>{{name | formartStr1 | formartStr2}}</p>
     </div>

```

##### 4.过滤器的注意点：

（1） 只能在插值语法和v-bind中使用

（2） 过滤器可以连续使用 就是一段数据 可以交给多个过滤器使用

##### 5.全局代码

```vue
<body>
<!--这里就是MVVM中的View-->
<div id="app">
    <!--Vue会把name交给指定的过滤器处理之后, 再把处理之后的结果插入到指定的元素中-->
    <p>{{name | formartStr1 | formartStr2}}</p>
</div>
<script>
    
        Vue.filter("formartStr1", function (value) {
        // console.log(value);
        value = value.replace(/学院/g, "大学");
        console.log(value);
        //必须返回你要处理的事件当中
        return value;
    });
    Vue.filter("formartStr2", function (value) {
        // console.log(value);
        value = value.replace(/大学/g, "幼儿园");
            console.log(value);
        return value;
    });
    // 这里就是MVVM中的View Model
    let vue = new Vue({
        el: '#app',
        // 这里就是MVVM中的Model
        data: {
            name: "知播渔学院, 指趣学院, 前端学院, 区块链学院"
        },
        // 专门用于存储监听事件回调函数
        methods: {
        },
        // 专门用于定义计算属性的
        computed: {
        }
    });
</script>
</body>
```

##### 6.自定义一个局部过滤器

给创建Vue实例时传递的对象添加
filters: {
   key: 过滤器名称
  value: 过滤器处理函数
  'formartStr': function (value) {}

【当然定义的局部过滤器只能在我们所创建的VUE实例里面使用 在外面无法使用，过滤器在使用中 也可以传入参数】

```javascript
<div id="app2">
    <p>{{name | formartStr}}</p>
</div>
<script>
  
    let vue2 = new Vue({
        el: '#app2',
        // 这里就是MVVM中的Model
        data: {
            name: "知播渔学院, 指趣学院, 前端学院, 区块链学院"
        },
        // 专门用于存储监听事件回调函数
        methods: {
        },
        // 专门用于定义计算属性的
        computed: {
        },
        // 专门用于定义局部过滤器的
        filters: {
            "formartStr": function (value) {
                // console.log(value);
                value = value.replace(/学院/g, "大学");
                // console.log(value);
                return value;
            }
        }
    });
</script>
```

#### 小案例 图书购买

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border: 1px solid #e9e9e9;
            border-collapse: collapse;
            border-spacing: 0;
        }
        th,
        td {
            padding: 8px 16px;
            border: 1px solid #e9e9e9;
            text-align: left;
        }
        th {
            background-color: #f7f7f7;
            color: #5c6b77;
            font-weight: 600;
        }
    </style>
    <script src="../mypractice/node_modules/vue/dist/vue.js"></script>
</head>

<body>
    <div id="app">
        <div v-if="books.length">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>书籍名称</th>
                        <th>出版日期</th>
                        <th>价格</th>
                        <th>购买数量</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, index) in books">
                        <td>{{item.id}}</td>
                        <td>{{item.name}}</td>
                        <td>{{item.date}}</td>
                        <td>{{item.price | showPrice}}</td>
                        <td>
                            <!-- 按钮的属性 disabled 如果添加这个属性 那个按钮就会变成灰色的 无法点击 -->
                            <button @click="decrement(index)" v-bind:disabled="item.count <= 1">-</button>
                            {{item.count}}
                            <button @click="increment(index)">+</button>
                        </td>
                        <td><button @click="removeHandle(index)">移除</button></td>
                    </tr>
                </tbody>
            </table>
            <h2>总价格: {{totalPrice | showPrice}}</h2>
        </div>
        <h2 v-else>购物车为空</h2>
    </div>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                books: [
                    {
                        id: 1,
                        name: '《算法导论》',
                        date: '2006-9',
                        price: 85.00,
                        count: 1
                    },
                    {
                        id: 2,
                        name: '《UNIX编程艺术》',
                        date: '2006-2',
                        price: 59.00,
                        count: 1
                    },
                    {
                        id: 3,
                        name: '《编程珠玑》',
                        date: '2008-10',
                        price: 39.00,
                        count: 1
                    },
                    {
                        id: 4,
                        name: '《代码大全》',
                        date: '2006-3',
                        price: 128.00,
                        count: 1
                    },
                ]
            },
            methods: {
                increment(index) {
                    this.books[index].count++
                },
                decrement(index) {
                    this.books[index].count--
                },
                removeHandle(index) {
                    this.books.splice(index, 1)
                }
            },
            computed: {
                totalPrice() {
                    let totalPrice = 0
                    for (let i = 0; i < this.books.length; i++) {
                        //如果不写上+=的话 那么就是最后一样书一直在输出最终价格
                        totalPrice += this.books[i].price * this.books[i].count
                    }
                    return totalPrice
                }
            },
            filters: {
                 //表示价格后面.00
                showPrice(price) {
                    return '¥' + price + ".00"
                }
            }
        })
    </script>
</body>
</html>
```

输出结果

![1618047065648](./media/1618047065648.png)

#### 编程范式：1.命令式编程 2.声明式编程

1.命令式编程：第一步 第二步 需要做什么【命令做什么】

2.声明式编程：拿到数据 做一个声明 比如V-For 他会自己帮你遍历 而不是那种 你指定下一步都需要做什么

#### V-model

​      很多时候用来绑定表单的 配合表单使用的       

##### v-model的基本使用  双向绑定

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <input type="text" v-model="message">
  {{message}}
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>

</body>
</html>
```

##### v-model双向绑定原理实现

**textarea多行文本输入框**

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <!--<input type="text" v-model="message">-->
    
  <!--<input type="text" :value="message" @input="valueChange">-->
    <!--通过input实现获取到我们所输入的内容-->
  <input type="text" :value="message" @input="message = $event.target.value">
     <textarea v-model="message"> </textarea>
  <h2>{{message}}</h2>
</div>
<script src="./node_modules/vue/dist/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    },
    methods: {
      valueChange(event) {
        this.message = event.target.value;
      }
    }
  })
</script>
</body>
</html>
```

输出结果

![1618118430776](./media/1618118430776.png)

##### V-model结合radio使用

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<div id="app">
  <!-- v-model 两个radio标签绑定一个变量的话 也是互斥的关系 -->
    <!--如果不使用v-model的话 使用name标签也是可以互斥的·-->
    <!--input标签中value的值是必须添加的-->
  <label for="male">
    <input type="radio" id="male" value="男" v-model="sex">男
  </label>
  <label for="female">
    <input type="radio" id="female" value="女" v-model="sex">女
  </label>
  <h2>您选择的性别是: {{sex}}</h2>
</div>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      sex: '女'
    }
  })
</script>

</body>
</html>
```

输出结果

![1618119325852](./media/1618119325852.png)

##### V-model结合checkbox使用

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <!--1.checkbox单选框-->
    
  <!--<label for="agree">-->
    <!--<input type="checkbox" id="agree" v-model="isAgree">同意协议-->
  <!--</label>-->
  <!--<h2>您选择的是: {{isAgree}}</h2>-->
  <!--<button :disabled="!isAgree">下一步</button>-->
    
    
  <!--2.checkbox多选框-->
  <input type="checkbox" value="篮球" v-model="hobbies">篮球
  <input type="checkbox" value="足球" v-model="hobbies">足球
  <input type="checkbox" value="乒乓球" v-model="hobbies">乒乓球
  <input type="checkbox" value="羽毛球" v-model="hobbies">羽毛球
  <h2>您的爱好是: {{hobbies}}</h2>
    
  <label v-for="item in originHobbies" :for="item">
    <input type="checkbox" :value="item" :id="item" v-model="hobbies">{{item}}
  </label>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      isAgree: false, // 单选框
      hobbies: [], // 多选框,
      originHobbies: ['篮球', '足球', '乒乓球', '羽毛球', '台球', '高尔夫球']
    }
  })
</script>
</body>
</html>
```

输出结果

![1618129376308](./media/1618129376308.png)

##### V-model的修饰符

**lazy修饰符：**
默认情况下，v-model默认是在input事件中同步输入框的数据的。
也就是说，一旦有数据发生改变对应的data中的数据就会自动发生改变。
lazy修饰符可以让数据在失去焦点或者回车时才会更新：
**number修饰符：**
默认情况下，在输入框中无论我们输入的是字母还是数字，都会被当做字符串类型进行处理。
但是如果我们希望处理的是数字类型，那么最好直接将内容当做数字处理。
number修饰符可以让在输入框中输入的内容自动转成数字类型：
**trim修饰符：**
如果输入的内容首尾有很多空格，通常我们希望将其去除
trim修饰符可以过滤内容左右两边的空格                                                                           

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <!--1.修饰符: lazy-->
  <input type="text" v-model.lazy="message">
  <h2>{{message}}</h2>

  <!--2.修饰符: number-->
  <input type="number" v-model.number="age">
  <h2>{{age}}-{{typeof age}}</h2>

  <!--3.修饰符: trim-->
  <input type="text" v-model.trim="name">
  <h2>您输入的名字:{{name}}</h2>
</div>
<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      age: 0,
      name: ''
    }
  })
</script>
</body>
</html>
```

输出结果

![1618131686337](./media/1618131686337.png)

### 组件化

##### 什么是组件化？

如果我们将一个页面中所有的处理逻辑全部放在一起，处理起来就会变得非常复杂，而且不利于后续的管理以及扩展。但如果，我们讲一个页面拆分成一个个小的功能块，每个功能块完成属于自己这部分独立的功能，那么之后整个页面的管理和维护就变得非常容易了。

组件化是Vue.js中的重要思想
它提供了一种抽象，让我们可以开发出一个个独立可复用的小组件来构造我们的应用。任何的应用都会被抽象成一颗组件树。

![1618143412002](./media/1618143412002.png)

##### 组件使用的三个步骤

1.创建组件构造器
2.注册组件
3.使用组件

##### 如何使用组件

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body> 
<div id="app">
  <!--3.使用组件-->
  <my-cpn></my-cpn>
  <my-cpn></my-cpn>
  <my-cpn></my-cpn>
  <my-cpn></my-cpn>

  <div>
    <div>
      <my-cpn></my-cpn>
    </div>
  </div>
</div>
//在我们所使用的入口之外的地方调用组件是不可以的
<my-cpn></my-cpn>

<script src="../js/vue.js"></script>
<script>
  // 1.创建组件构造器对象  Vue.extend()通过这个名字可以注册一个组件
  const cpnC = Vue.extend({
    template: `
      <div>
        <h2>我是标题</h2>
        <p>我是内容, 哈哈哈哈</p>
        <p>我是内容, 呵呵呵呵</p>
      </div>`
  })
  // 2.注册组件
  //'my-cpn'组建的名称 后面的是组建的构造器
  Vue.component('my-cpn', cpnC)
    
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>
</body>
</html>
```

输出结果

![1618145394719](./media/1618145394719.png)

##### 全局组件和局部组件

```vue
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
<div id="app">
  <cpn></cpn>
  <cpn></cpn>
  <cpn></cpn>
</div>
<div id="app2">
  <cpn></cpn>
</div>
<script src="../js/vue.js"></script>
<script>
  // 1.创建组件构造器
  const cpnC = Vue.extend({
    template: `
      <div>
        <h2>我是标题</h2>
        <p>我是内容,哈哈哈哈啊</p>
      </div>
    `
  })
  // 2.注册组件(全局组件, 意味着可以在多个Vue的实例下面使用)
  // Vue.component('cpn', cpnC)
  
  // 疑问: 怎么注册的组件才是局部组件了?
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    },
    components: {
      // cpn使用组件时的标签名
      cpn: cpnC
    }
  })
  const app2 = new Vue({
    el: '#app2'
  })
</script>
</body>
</html>
```

##### 父组件和子组件

子组件可以在父组件中注册

eg：

```vue
<body>

<div id="app">
  <cpn2></cpn2>
  <!--<cpn1></cpn1>-->
</div>

<script src="../js/vue.js"></script>
<script>
  // 1.创建第一个组件构造器(子组件)
  const cpnC1 = Vue.extend({
    template: `
      <div>
        <h2>我是标题1</h2>
        <p>我是内容, 哈哈哈哈</p>
      </div>
    `
  })


  // 2.创建第二个组件构造器(父组件)
  const cpnC2 = Vue.extend({
    template: `
      <div>
        <h2>我是标题2</h2>
        <p>我是内容,呵呵呵呵</p>
        <cpn1></cpn1>
      </div>
    `,
      //在父组件中注册，cpn1 就可以显示出来
    components: {
      cpn1: cpnC1 
    }
  })

  // root组件
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    },
    components: {
      cpn2: cpnC2
    }
  })
</script>
</body>
```

##### 注册组件语法糖

```vue
<body>

<div id="app">
  <cpn1></cpn1>
  <cpn2></cpn2>
</div>

<script src="../js/vue.js"></script>
<script>
  // 1.全局组件注册的语法糖
  // 1.创建组件构造器
  // const cpn1 = Vue.extend()

  // 2.注册组件
  Vue.component('cpn1', {
    template: `
      <div>
        <h2>我是标题1</h2>
        <p>我是内容, 哈哈哈哈</p>
      </div>
    `
  })
  // 2.注册局部组件的语法糖
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    },
    components: {
      'cpn2': {
        template: `
          <div>
            <h2>我是标题2</h2>
            <p>我是内容, 呵呵呵</p>
          </div>
    `
      }
    }
  })
</script>
</body>
```

##### 2种分离模板的方法

1.

```javascript
<!--1.script标签, 注意:类型必须是text/x-template-->
<script type="text/x-template" id="cpn">-->
<div>
  <h2>我是标题</h2>
  <p>我是内容,哈哈哈</p>
</div>
</script>
```

2.

```javascript
<!--2.template标签-->
<template id="cpn">
  <div>
    <h2>我是标题</h2>
    <p>我是内容,呵呵呵</p>
  </div>
</template>
```

```javascript
 // 1.注册一个全局组件
  Vue.component('cpn', {
    template: '#cpn'
  })

```

#### 为什么组件中的data必须是函数

****

**组件中也有自己的data属性，但是这个属性必须是函数，也可以有methods函数，也有自己的周期函数，其实组件的原型也是指向vue的，**

因为如果不是一个函数的话，组件实列对象调用的组件中的data所有返回都是一个值，这样我们多次调用组件的话，就会形成一个变了全都变的情况，如果我们使用函数的话，每一次函数调用就是单独返回一个data。

**官方语言：**

【 “组件中的data写成一个函数,数据以函数返回值形式定义,这样每复用一次组件,就会返回一份新的data,类似于给每个组件实例创建一个私有的数据空间,让各个组件实例维护各自的数据。而单纯的写成对象形式,就使得所有组件实例共用了一份data,就会造成一个变了全都会变的结果。” 】

##### 1.data如果是一个对象的形式

```JavaScript
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<!--组件实例对象-->
<div id="app">
  <cpn></cpn>
  <cpn></cpn>
  <cpn></cpn>
</div>

<template id="cpn">
  <div>
    <h2>当前计数: {{counter}}</h2>
    <button @click="increment">+</button>
    <button @click="decrement">-</button>
  </div>
</template>
<script src="../js/vue.js"></script>
<script>
  // 1.注册组件
  const obj = {
    counter: 0
  }
  Vue.component('cpn', {
    template: '#cpn',
  
     data() {
       return obj
     },
    methods: {
      increment() {
        this.counter++
      },
      decrement() {
        this.counter--
      }
    }
  })

  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>
</body>
</html>
```

输出结果

![1618374459933](./media/1618374459933.png)

##### 2.data是一个函数形式

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>

<!--组件实例对象-->
<div id="app">
  <cpn></cpn>
  <cpn></cpn>
  <cpn></cpn>
</div>

<template id="cpn">
  <div>
    <h2>当前计数: {{counter}}</h2>
    <button @click="increment">+</button>
    <button @click="decrement">-</button>
  </div>
</template>
<script src="../js/vue.js"></script>
<script>
  // 1.注册组件
  const obj = {
    counter: 0
  }
  Vue.component('cpn', {
    template: '#cpn',
     data() {
       return {
         counter: 0
       }
     },
    methods: {
      increment() {
        this.counter++
      },
      decrement() {
        this.counter--
      }
    }
  })
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    }
  })
</script>
</body>
</html>
```

输出结果

![1618374613349](./media/1618374613349.png)

## 组件间通信方式

1. props 父组件向子组件传递数据
2. $emit 自定义事件
3. slot 插槽分发内容 

#### 父子组件之间的通信

子组件是不能引用父组件或者Vue实例的数据的

##### 为什么要进行父子附件之间的通信？

比如在一个页面中，我们从服务器请求到了很多的数据。其中一部分数据，并非是我们整个页面的大组件来展示的，而是需要下面的子组件进行展示。这个时候，并不会让子组件再次发送一个网络请求，而是直接让大组件(父组件)将数据传递给小组件(子组件)

props：properties：属性

父组件传子组件 用props

子组件通过事件向父组件发送消息 $emit 

###### 父组件向子组件传入事件

props传值有三种方式【在引用组件时，通过 v-bind 动态赋值      父组件之中v-bind绑定传值 】

方式1：指定传递属性名，注意是 数组形式

![1618459372285](./media/1618459372285.png)

方式2：指定传递属性名和数据类型，注意是 对象形式

【支持以下限制：String
Number
Boolean
Array
Object
Date
Function
Symbol】

![1618459432089](./media/1618459432089.png)

方式3：指定属性名、数据类型、必要性、默认值

![1618459443146](./media/1618459443146.png)

default的意思就是别人不往里面传值的话，他的默认值就是我们在default里面传入的值

 必要性（require）是一个布尔值 只能传入true和false

 类型是对象或者数组时, 默认值必须是一个函数【不常用】

![1618462731372](./media/1618462731372.png)f

**父传子props之中必须遵循驼峰样式命名如果有大写就必须加上-这个符号分隔开**

##### 父组件传值子组件案例

```javascript
<body>
<div id="app">
  <cpn :c-info="info" :child-my-message="message" v-bind:class></cpn>
</div>
<template id="cpn">
  <div>
    <h2>{{cInfo}}</h2>
    <h2>{{childMyMessage}}</h2>
  </div>
</template>

<script src="../js/vue.js"></script>
<script>
//子组件
  const cpn = {
    template: '#cpn',
    props: {
      cInfo: {
        type: Object,
        default() {
          return {}
        }
      },
      childMyMessage: {
        type: String,
        default: ''
      }
    }
  }
//父组件
  const app = new Vue({
    el: '#app',
    data: {
      info: {
        name: 'why',
        age: 18,
        height: 1.88
      },
      message: 'aaaaaa'
    },
    components: {
      cpn
    }
  })
</script>

</body>
```

![1618486603462](./media/1618486603462.png)

##### 子组件向父组件传值

通过自定义事件传值【通常我们在子组件中发生什么事情，父组件监听到】

什么时候需要自定义事件呢？
当子组件需要向父组件传递数据时，就要用到自定义事件了。
我们之前学习的v-on不仅仅可以用于监听DOM事件，也可以用于组件间的自定义事件。
自定义事件的流程：
在子组件中，通过$emit()来触发事件。
在父组件中，通过v-on来监听子组件事件。

案例

```javascript
<body>
    <div id="app">
  <study @respect="longskr"></study>
    </div>
    <script src="./node_modules/vue/dist/vue.js"></script>
    <script src="./子组件传父组件.js"></script>
    <script>
        //父组件
        let app =new Vue({
            el:"#app",
            data:{
                message:"你好啊DDDDDDD"
            },
            components:{
           
               study
            },
            methods:{
                longskr(item){
                  console.log("longskr",item);
                }
            }
        })
    </script>
</body>
```

子组件

```javascript
;(function(){
    window.study={
        template:`
        <div>
        <button v-for="item in categories"
            @click="btnClick(item)">
          {{item.name}}
        </button>
        </div> `,
        data() {
            return {
              categories: [
                {id: 'aaa', name: '热门推荐'},
                {id: 'bbb', name: '手机数码'},
                {id: 'ccc', name: '家用家电'},
                {id: 'ddd', name: '电脑办公'},
              ]
            }
          },
          methods:{
              btnClick(item){
                  this.$emit("respect",item)
              }
          }
    }
})()
```

输出结果

![1618648601462](./media/1618648601462.png)

#### 使用组件的两种方法

<cpn> </cpn>

<cpn/>

两种方法都可以使用组件

#### 父子组件通信结合双向绑定案例

案例需求

【

1.用父子之间的通信获取父组件中data中的值

2.通过双向绑定当改变input输入框的时候，也改变父组件中的data的值

3.下面的显示输入的值永远是上面的一百倍

 】

```javascript
<body>
<div id="app">
  <cpn :number1="num1"
       :number2="num2"
       @num1change="num1change"
       @num2change="num2change"/>
</div
<template id="cpn">
  <div>
    <h2>props:{{number1}}</h2>
    <h2>data:{{dnumber1}}</h2>
    <input type="text" :value="dnumber1" @input="num1Input">
    <h2>props:{{number2}}</h2>
    <h2>data:{{dnumber2}}</h2>
    <input type="text" :value="dnumber2" @input="num2Input">
  </div>
</template>
<script src="../js/vue.js"></script>
<script>
  //父组件
  const app = new Vue({
    el: '#app',
    data: {
      num1: 1,
      num2: 0
    },
    methods: {
      //传进来一个value 对value取值
      num1change(value) {
        this.num1 = parseFloat(value)
      },
      num2change(value) {
        this.num2 = parseFloat(value)
      }
    },
    //子组件
    components: {
      cpn: {
        template: '#cpn',
        props: {
          number1: Number,
          number2: Number
        },
        data() {
          return {
            dnumber1: this.number1,
            dnumber2: this.number2
          }
        },
        methods: {
          num1Input(event) {
            // 1.将input中的value赋值到dnumber中
            this.dnumber1 = event.target.value;

            // 2.为了让父组件可以修改值, 发出一个事件
            this.$emit('num1change', this.dnumber1)

            // 3.同时修饰dnumber2的值
            this.dnumber2 = this.dnumber1 * 100;
            this.$emit('num2change', this.dnumber2);
          },
          num2Input(event) {
            this.dnumber2 = event.target.value;
            this.$emit('num2change', this.dnumber2)

            // 同时修饰dnumber2的值
            this.dnumber1 = this.dnumber2 / 100;
            this.$emit('num1change', this.dnumber1);
          }
        }
      }
    }
  })
</script>
</body>
```

输出结果

![1618719493680](./media/1618719493680.png)

解析：

**需求二：通过双向绑定当改变input输入框的时候，也改变父组件中的data的值**

我们 在模板中通过v-bind绑定到data上面的数据dnumber， 然后通过v-on绑定input 通过$event.target.value来获取到input上面的值，1.将input中的value赋值到dnumber中    

```javascript
this.dnumber1 = event.target.value; 
  // 2.为了让父组件可以修改值, 发出一个事件
    this.$emit('num1change', this.dnumber1)
```

在通过$emit将我们获取到的值和dnumber传回父组件,父组件通过v-on绑定我们传回来的

'num1change 来监听一个方法 这样就可以修改父组件中的值了

```javascript
 <div id="app">
  <cpn :number1="num1"
       :number2="num2"
       @num1change="num1change"
       @num2change="num2change"/>
</div>

 
 //父组件
  const app = new Vue({
    el: '#app',
    data: {
      num1: 1,
      num2: 0
    },
    methods: {
      //传进来一个value 对value取值
      num1change(value) {
        this.num1 = parseFloat(value)
      },
      num2change(value) {
        this.num2 = parseFloat(value)
      }
    },
```

#### 父子组件通信通过（watch）实现

```javascript
<body>

<div id="app">
  <cpn :number1="num1"
       :number2="num2"
       @num1change="num1change"
       @num2change="num2change"/>
</div>

<template id="cpn">
  <div>
    <h2>props:{{number1}}</h2>
    <h2>data:{{dnumber1}}</h2>
    <input type="text" v-model="dnumber1">
    <h2>props:{{number2}}</h2>
    <h2>data:{{dnumber2}}</h2>
    <input type="text" v-model="dnumber2">
  </div>
</template>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      num1: 1,
      num2: 0
    },
    methods: {
      num1change(value) {
        this.num1 = parseFloat(value)
      },
      num2change(value) {
        this.num2 = parseFloat(value)
      }
    },
    components: {
      cpn: {
        template: '#cpn',
        props: {
          number1: Number,
          number2: Number,
          name: ''
        },
        data() {
          return {
            dnumber1: this.number1,
            dnumber2: this.number2
          }
        },
        watch: {
          dnumber1(newValue) {
            this.dnumber2 = newValue * 100;
            this.$emit('num1change', newValue);
          },
          dnumber2(newValue) {
            this.number1 = newValue / 100;
            this.$emit('num2change', newValue);
          }
        }
      }
    }
  })
</script>

</body>
```

#### 父子组件之间直接访问方式

父组件中拿到子组件中的对象 而不是二者之间进行传值

##### 父组件访问子组件

**父组件访问子组件：使用$children或$refs**
**子组件访问父组件：使用$parent**

refs： reference(引用)

**我们在开发中一般不会使用$children属性拿到子组件里面的信息而是使用$refs这个属性** 但是我们要是直接打印这个属性，就会打印出一个空对象，所以我们在使用这个属性的时候，给子组件加上ref这个属性.

代码

###### $children 使用的时候必须加上你要的组件下标

```javascript
<body>
    <div id="app">
        <cpn></cpn>
        <cpn></cpn>
        <cpn></cpn>
        <button @click="btnClick">按钮</button>
    </div>

    <template id="cpn">
        <div>我是子组件</div>
      </template>

    <script src="../js/vue.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                message: '你好啊'
            },
            methods: {
                btnClick() {
                    console.log(this.$children)
                }
            },
            components: {
                cpn: {
                    template: '#cpn',
                    data() {
                        return {
                            name: '我是子组件的name',
                            level: "sssssss"
                        }
                    },
                    methods: {
                        showMessage() {
                            console.log('showMessage');
                        }
                    }
                }
            }
        })
    </script>
</body>
```

输出结果

![1618739208139](./media/1618739208139.png)

![1618739218516](./media/1618739218516.png)

如果我们要输出我们在子组件中的某一个部分的内容 就可以在父组件 指定第几个组件和里面的内容

```javascript
<body>
    <div id="app">
        <cpn></cpn>
        <cpn></cpn>
        <cpn></cpn>
        <button @click="btnClick">按钮</button>
    </div>

    <template id="cpn">
        <div>我是子组件</div>
      </template>

    <script src="../js/vue.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                message: '你好啊'
            },
            methods: {
                btnClick() {
                    console.log(this.$children)
                    console.log(this.$children[1].showMessage)
                    console.log(this.$children[1].name)
                }
            },
            components: {
                cpn: {
                    template: '#cpn',
                    data() {
                        return {
                            name: '我是子组件的name',
                            level: "sssssss"
                        }
                    },
                    methods: {
                        showMessage() {
                            console.log('showMessage');
                        }
                    }
                }
            }
        })
    </script>
</body>
```

输出结果

![1618739599502](./media/1618739599502.png)

###### $refs

由上可见我们可以拿到子组件里面的内容，但是如果多次调用组件，【我们在开发过程中，需要往里面填入别的组件】就无法通过索引的方式拿到我们指定要的子组件的内容了，所以我们就需要使用$refs这种方法

```JavaScript
<body>
    <div id="app">
        <cpn></cpn>
        <cpn></cpn>
        <cpn></cpn>

        <cpn ref="AAA"></cpn>
        <button @click="btnClick">按钮</button>
    </div>

    <template id="cpn">
        <div>我是子组件</div>
      </template>

    <script src="../js/vue.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                message: '你好啊'
            },
            methods: {
                btnClick() {
                    // 2.$refs => 对象类型, 默认是一个空的对象 ref='bbb'
                console.log(this.$refs.AAA.level);
                }
            },
            components: {
                cpn: {
                    template: '#cpn',
                    data() {
                        return {
                            name: '我是子组件的name',
                            level: "sssssss"
                        }
                    },
                    methods: {
                        showMessage() {
                            console.log('showMessage');
                        }
                    }
                }
            }
        })
    </script>
</body>
```

输出结果

![ ](./media/1618739911546.png)

**通过给组件绑定一个ref=“AAA”就可以指定我们要获取哪一个子组件其中的内容了，**

**在输出的时候就可以用this.$refs.AAA.level来输出我们要的子组件的内容**

**小提示：在子组件上面加的属性切记是 ref=”xxx“ 不是refs=”xxx“ 在输出的时候是this.$refs**

##### 子组件访问父组件【不常用 $root可能会用到】

###### $root

```javascript
<body>

<div id="app">
  <cpn></cpn>
</div>

<template id="cpn">
  <div>
    <h2>我是cpn组件</h2>
    <ccpn></ccpn>
  </div>
</template>

<template id="ccpn">
  <div>
    <h2>我是子组件</h2>
    <button @click="btnClick">按钮</button>
  </div>
</template>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    },
    components: {
      cpn: {
        template: '#cpn',
        data() {
          return {
            name: '我是cpn组件的name'
          }
        },
        components: {
          ccpn: {
            template: '#ccpn',
            methods: {
              btnClick() {
                // 2.访问根组件$root
                console.log(this.$root);
                console.log(this.$root.message);
              }
            }
          }
        }
      }
    }
  })
</script>

</body>
```

输出结果

![1618812130786](./media/1618812130786.png)

![1618812137696](./media/1618812137696.png)

输出的是根组件 $root指向的是根组件 也就是父组件Vue实例

###### $parent访问父组件 

单层父组件

```javascript
<body>

<div id="app">
  <cpn></cpn>
</div>

<template id="cpn">
  <div>
    <h2>我是cpn组件</h2>
    <ccpn></ccpn>
  </div>
</template>

<template id="ccpn">
  <div>
    <h2>我是子组件</h2>
    <button @click="btnClick">按钮</button>
  </div>
</template>

<script src="../js/vue.js"></script>
<script>
    //父组件
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊'
    },
      //子组件【1】
    components: {
      cpn: {
        template: '#cpn',
        data() {
          return {
            name: '我是cpn组件的name'
          }
        },
          //子组件【2】
        components: {
          ccpn: {
            template: '#ccpn',
            methods: {
              btnClick() {
                // 1.访问父组件$parent
                 console.log(this.$parent);
                 console.log(this.$parent.name);
              }
            }
          }
        }
      }
    }
  })
</script>

</body>
```

输出结果

![1618812369157](./media/1618812369157.png)

![1618812377217](./media/1618812377217.png)

因为我们是父组件中套了一层子组件，然后在【1】子组件中又套了一层【2】子组件我们的访问this.$parent是在【2】子组件中 所以访问到【1】子组件中data的name属性

#### slot插槽

组件的插槽也是为了让我们封装的组件更加具有扩展性。让使用者可以决定组件内部的一些内容到底展示什么。

##### 1.slot插槽基本使用

【组件中传递标签的方法】

1.插槽的基本使用 <slot></slot>
2.插槽的默认值 <slot>button</slot>
3.如果有多个值, 同时放入到组件进行替换时, 一起作为替换元素

在模板中定义<slot></slot>插槽 我们在组件中，写入我们想要写入插槽中的东西，如果有多个写入值的话，就全部替换到插槽中,我们在slot插槽中，定义我们默认的标签【或者内容】，就会默认显示在组件之中，在往里面在写入值就会替换掉我们原来写入的默认值。

```javascript
<body>
    <div id="app">
        
         <cpn>
             <i>heihei嘿</i>
         </cpn>

         <cpn>
             <b>那就这样吧</b>
         </cpn>
         <cpn>
             <h2>这里面是标题</h2>
         </cpn>
         <cpn></cpn>
         <cpn></cpn>
    </div>

    <template id="cpn">
        <div>
            <h2>我是组件</h2>
            <p>我是组件, 哈哈哈</p>
            <slot><button>嘿嘿</button></slot>
            <!--<button>按钮</button>-->
        </div>
    </template>

    <script src="../js/vue.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                message: '你好啊'
            },
            components: {
                cpn: {
                    template: '#cpn'
                }
            }
        })
    </script>

</body>
```

输出结果

![1618823881658](./media/1618823881658.png)

##### 2.slot-具名插槽的使用

```javascript
<body>
    <div id="app">
        <cpn><span>嘻嘻</span></cpn>
        <cpn><i slot="center">勒勒</i></cpn>
    </div>

    <template id="cpn">
        <div>
            <slot name="left"><span>左边</span></slot>
            <slot  name="center"><span>中间</span></slot>
            <slot  name="right"><span>右边</span></slot>

             <slot> <span>jiji</span> </slot>
        </div>
    </template>

    <script src="./node_modules/vue/dist/vue.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                message: '你好啊'
            },
            components: {
                cpn: {
                    template: '#cpn'
                }
            }
        })
    </script>
</body>
```

输出结果

![1618827380507](./media/1618827380507.png)

【模板中定义插槽 ，并且赋予name的名字，在使用的过程中，我们可以给我们要插入的组件中给一个slot=“xxx”的属性 就可以替换了,如果定义插槽没给name属性，就会被我们组件中写的默认替换掉】

##### 3.编译作用域

简单用代码表示一下 什么是编译作用域

```javascript
<body>
<div id="app">
  <cpn v-show="isShow"></cpn>
</div>

<template id="cpn">
  <div>
    <h2>我是子组件</h2>
    <p>我是内容, 哈哈哈</p>
    <button v-show="isShow">按钮</button>
  </div>
</template>

<script src="../js/vue.js"></script>
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      isShow: true
    },
    components: {
      cpn: {
        template: '#cpn',
        data() {
          return {
            isShow: false
          }
        }
      },
    }
  })
</script>
</body>
```

输出结果：

![1618834286750](./media/1618834286750.png)

解析：我们在子组件中定义了isShow ，同时我们在他的父组件Vue实例中也定义了isShow，并且在组件中 v-show 使用了这个isShow，如果按照我们之前的理解我们使用的isShow是使用的自己子组件中的啊，但是为什么会使用他的父组件实列中的isShow呢？就是我们在子组件中使用变量的时候，他会先看你使用的这个变量是在那个模板里面的  就比如说 我们上面的子组件cpn属于Vue实例里面，所以首先先去Vue实例里面寻找我们这个isShow。这就是**编译作用域**

**父组件模板的所有东西都会在父级作用域内编译；子组件模板的所有东西都会在子级作用域内编译。**

##### **4.作用域插槽【大致了解就可以 】

父组件替换插槽的标签，但是内容由子组件来提供。

【简单来说，就是我们想要子组件的数据 通过插槽来实现 并且在组件中给一个根元素 ，给一个特定的属性  slot-scope="slot", ，在我们的组件中模板中绑定一个 :xxx="我们要的数据", 这样就可以在我们组件中调用了】

```javascript
<body>
    <div id="app">
        
        <cpn> </cpn>
        
        <cpn>
            <div slot-scope="slot">
                <span v-for="item in slot.data">
                    -{{item}}-
                </span>
            </div>
        </cpn>

        <cpn>
            <template slot-scope="slot">
                 <span>{{slot.data.join("^")}}</span>
            </template>
        </cpn>
        
    </div>

    <template id="cpn">
        <div>
            <slot :data="pLanguages">
                <ul>
                    <li v-for="item in pLanguages">{{item}}</li>
                </ul>
            </slot>
        </div>
    </template>
    <script src="./node_modules/vue/dist/vue.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                message: '你好啊'
            },
            components: {
                cpn: {
                    template: '#cpn',
                    data() {
                        return {
                            pLanguages: ['JavaScript', 'C++', 'Java', 'C#', 'Python', 'Go', 'Swift']
                        }
                    }
                }
            }
        })
    </script>
</body>
```

输出结果

![1618912315374](./media/1618912315374.png)

#### CommonJS

但是直接写在JS之中没有功能去帮你解析CommonJS所以写在原生JS中是无意义的

Commonjs是作为Node中模块化规范以及原生模块面世的，他是服务端开发的一种规范，不适合用用于客户端，因为客户端使用COmmonJS的话会同步加载模块，会影响渲染时间影响客户体验

##### CommonJS的导出：

![1618980881059](./media/1618980881059.png)

##### CommonJS的导入

![1618980897473](./media/1618980897473.png)

#### ES6的导入和导出

##### 导出

```javascript
var name = '小明'
var age = 18
var flag = true

function sum(num1, num2) {
  return num1 + num2
}

if (flag) {
  console.log(sum(20, 30));
}

// 1.导出方式一:
export {
  flag, 
  sum,
}

// 2.导出方式二:命名式导出
export var num1 = 1000;
export var height = 1.88


// 3.导出函数/类
export function mul(num1, num2) {
  return num1 * num2
}

export class Person {
  run() {
    console.log('在奔跑');
  }
}

// 默认导出
// const address = '北京市'
// export default address
 
 
// 默认导出
export default  function (argument) {
  console.log(argument);
}
```

##### 导入

```javascript
// 1.导入的{}中定义的变量
import {flag, sum} from "./aaa.js";

if (flag) {
  console.log('小明是天才, 哈哈哈');
  console.log(sum(20, 30));
}

// 2.直接导入export定义的变量
import {num1, height} from "./aaa.js";
console.log(num1);
console.log(height);

// 3.导入 export的function/class
import {mul, Person} from "./aaa.js";

console.log(mul(30, 50));

const p = new Person();
p.run()

// 4.导入 export default中的内容
import addr from "./aaa.js";

addr('你好啊');

// 5.统一全部导入
// import {flag, num, num1, height, Person, mul, sum} from "./aaa.js";
/*aaa可以看作我们起的别名*/
// import * as aaa from './aaa.js' //*通配符全部的意思 导入到aaa对象之中

console.log(aaa.flag);
console.log(aaa.height);
```

# webpack

#### 什么是Webpack？

本质上，**webpack** 是一个用于现代 JavaScript 应用程序的 *静态模块打包工具*。当 webpack 处理应用程序时，它会在内部构建一个 [依赖图(dependency graph)](https://webpack.docschina.org/concepts/dependency-graph/)，此依赖图对应映射到项目所需的每个模块，并生成一个或多个 *bundle*。**webpack强大之处在于能自己处理各个模块之间的依赖**



**package.json：通过npm init生成的，npm包管理的文件（暂时没有用上，后面才会用上）**

#### Webpack官网

https://webpack.docschina.org/

#### Webpack的全局安装和本地安装

webpack对于全局安装**不推荐** 全局安装 webpack。全局安装的 webpack ，在打包项目的时候，使用的是你安装在自己电脑上的 webpack，如果项目到了另一个人的电脑上，他可能安的是旧版本 webpack。那么就可能涉及兼容性的问题。而且如果他没有在全局安装 webpack 则就无法打包。所以，为了解决以上的问题，官方推荐本地安装 webpack，就是将 webpack 安装到对应项目中。这样项目到哪里，webpack 就跟到哪里（webpack 打包工具随着项目走）;

#### 什么是npm

NPM的全称是Node Package Manager
是一个NodeJS包管理和分发工具，已经成为了非官方的发布Node模块（包）的标准。
后续我们会经常使用NPM来安装一些开发过程中依赖包.

# vue-cli

##### 安装脚手架

npm install -g @vue/cli

### ES6中this指向

**问题: 箭头函数中的this是如何查找的了?** 

  答案: 向外层作用域中, 一层层查找this, 直到有this的定义. 

   也就是说箭头函数的this引用的就是最近作用域中的this 

```JavaScript
 const obj = {
    aaa() {
      setTimeout(function () {
        
        setTimeout(function () {
          console.log(this); // window外层作用域使用的是 function(){}所以是window
        })

        setTimeout(() => {
          console.log(this); // window 外层也是window所以也是function
        })
      })

      setTimeout(() => {
        setTimeout(function () {
          console.log(this); // window 里面也是function 指向window
        })

        setTimeout(() => {
          console.log(this); // obj
        })
      })
    }
  }

  obj.aaa()
```

# vue-router

#### 什么是前端渲染？

SPA单页面富应用阶段 也可以称为前端路由页面

SPA：simple page web application 单页面富应用

整个网页也就一个html页面 将所有的vue页面打包到一个js里面

通过url来管理页面

其实SPA最主要的特点就是在前后端分离的基础上加了一层前端路由.
也就是前端来维护一套路由规则.

前端路由的核心是什么呢？
改变URL，但是页面不进行整体的刷新。页面整体刷新 代表回去服务器请求资源 所以我们页面不刷新 但是上打包的js请求 我们这个路由里面的东西渲染到网页中

#### 什么是后端渲染？

早期开发的时候没有JS使用jsp开发 也就是 java server page  将前面的路由地址请求发送到服务器，服务器解析出你要请求哪一个页面 然后直接用后端【Java】从数据库中读取数据，并且将他的动态直接渲染到页面中，然后直接将渲染好的网页从服务器返回到我们请求的页面上，但是里面只有CSSHTML等  这个就叫做服务端渲染  也就是后端渲染，

#### 什么是后端路由？ 

后端处理URL和页面之间的映射关系 就叫后端路由。

**早期的网站开发整个HTML页面是由服务器来渲染的，服务器直接生产渲染好对应的HTML页面, 返回给客户端进行展示.但是, 一个网站, 这么多页面服务器如何处理呢?一个页面有自己对应的网址 , 也就是URL，URL会发送到服务器, 服务器会通过正则对该URL进行匹配, 并且最后交给一个Controller进行处理，Controller进行各种处理, 最终生成HTML或者数据, 返回给前端.**
**这就完成了一个IO操作.**
**上面的这种操作, 就是后端路由.**

##### 后端路由的优点：

当我们页面中需要请求不同的路径内容时, 交给服务器来进行处理, 服务器渲染好整个页面, 并且将页面返回给客户顿.这种情况下渲染好的页面, 不需要单独加载任何的js和css, 可以直接交给浏览器展示, 这样也有利于SEO的优化.

##### 后端路由的缺点:

1一种情况是整个页面的模块由后端人员来编写和维护的.
2.另一种情况是前端开发人员如果要开发页面, 需要通过PHP和Java等语言来编写页面代码.
3.而且通常情况下HTML代码和数据以及对应的逻辑会混在一起, 编写和维护都是非常糟糕的事情..

#### 什么是前后端分离？

![image-20210507202430806](./media/image-20210507202430806.png)

##### 前后端分离阶段

1.随着Ajax的出现, 有了前后端分离的开发模式.
2后端只提供API来返回数据, 前端通过Ajax获取数据, 并且可以通过JavaScript将数据渲染到页面中.
3这样做最大的优点就是前后端责任的清晰, 后端专注于数据上, 前端专注于交互和可视化上.
4.并且当移动端(iOS/Android)出现后, 后端不需要进行任何处理, 依然使用之前的一套API即可。目前很多的网站依然采用这种模式开发...

#### 改变url保证页面不刷新的方法

###### 1.URL的hash

URL的hash也就是锚点(#), 本质上是改变window.location的href属性.
我们可以通过直接赋值location.hash来改变href, 但是页面不发生刷新

![image-20210509150541648](./media/image-20210509150541648.png)

###### 2.HTML5的history模式：pushState

history接口是HTML5新增的, 它有五种模式改变URL而不刷新页面.
history.pushState() 

**pushstate里面三个参数 第一个是对象参数 第二个是 title参数 第三个是 url地址**

 push方法 压入栈底 就是说 先是foo 上面再是/ 非常类似一个栈结构 栈顶部永远都是最新的URL

 可以返回上一层 history.back返回上一层

![image-20210509150657893](./media/image-20210509150657893.png)

###### 3.HTML5的history模式：replaceState

history.replaceState() 也可以改变url但是不刷新页面，但是这个是替换 不可以返回

![image-20210509150749972](./media/image-20210509150749972.png)

###### 4.HTML5的history模式：go

**补充说明：**
**上面只演示了三个方法**
**因为 history.back() 等价于 history.go(-1)**
**history.forward() 则等价于 history.go(1)**
**这三个接口等同于浏览器界面的前进后退。**

![image-20210509150913777](./media/image-20210509150913777.png)

**目前前端流行的三大框架, 都有自己的路由实现:**
Angular的ngRouter
React的ReactRouter
Vue的vue-router

**Vue-router**

vue-router是Vue.js官方的路由插件，它和vue.js是深度集成的，适合用于构建单页面应用。
我们可以访问其官方网站对其进行学习: https://router.vuejs.org/zh/
vue-router是基于路由和组件的
路由用于设定访问路径, 将路径和组件映射起来.
在vue-router的单页面应用中, 页面的路径的改变就是组件的切换.

#### 步骤一: 安装vue-router

npm install vue-router --save

#### 步骤二: 在模块化工程中使用它

(因为是一个插件, 所以可以通过Vue.use()来安装路由功能)

第一步：导入路由对象，并且调用 Vue.use(VueRouter)
第二步：创建路由实例，并且传入路由映射配置
第三步：在Vue实例中挂载创建的路由实例

#### 使用vue-router的步骤:

第一步: 创建路由组件
第二步: 配置路由映射: 组件和路径映射关系
第三步: 使用路由: 通过<router-link>和<router-view>

#### vue-router具体使用

###### 1.创建router实例

![image-20210509171908781](./media/image-20210509171908781.png)

###### 2.挂载到Vue实例中

![image-20210509172024761](./media/image-20210509172024761.png)

###### 3.创建路由组件

![image-20210509172051369](./media/image-20210509172051369.png)

###### 4.配置组件和路径的映射关系

![image-20210509172115075](./media/image-20210509172115075.png)

###### 5.使用路由

![image-20210509172146839](./media/image-20210509172146839.png)

<router-link>: 该标签是一个vue-router中已经内置的组件, 它会被渲染成一个<a>标签. 通过tag来改变我们想要的标签
<router-view>: 该标签会根据当前的路径, 动态渲染出不同的组件.
网页的其他内容, 比如顶部的标题/导航, 或者底部的一些版权信息等会和<router-view>处于同一个等级.
在路由切换时, 切换的是<router-view>挂载的组件, 其他内容不会发生改变.

###### 6.最佳效果如下

![image-20210509172314995](./media/image-20210509172314995.png)

#### 如何改变路由的默认路径

我们这里还有一个不太好的实现:默认情况下, 进入网站的首页, 我们希望<router-view>渲染首页的内容.但是我们的实现中, 默认没有显示首页组件, 必须让用户点击才可以.如何可以让路径默认跳到到首页, 并且<router-view>渲染首页组件呢?
非常简单, 我们只需要配置多配置一个映射就可以了.在router文件夹下面的 index.js之中

![image-20210509172426019](./media/image-20210509172426019.png)

**配置解析:**
我们在routes中又配置了一个映射. 
path配置的是根路径: /
redirect是重定向, 也就是我们将根路径重定向到/home的路径下, 这样就可以得到我们想要的结果了.

#### HTML5中的History模式

在前面说到 改变url并且保证页面不刷新的方法有两种 

###### 1.URL中的hash

###### 2.HTML5中的history

在默认的情况下, 路径的改变使用的URL的hash.

如果希望使用HTML5的history模式, 非常简单, 进行如下配置即可:

![image-20210509173157335](./media/image-20210509173157335.png)

切换后的效果

![image-20210509173225251](./media/image-20210509173225251.png)

#### router-link的补充  默认渲染成A标签

**1.在前面的<router-link>中, 我们只是使用了一个属性: to, 用于指定跳转的路径..**

<router-link>还有一些其他属性:  <router-link to='/home' tag='li'>

**2.tag: tag可以指定<router-link>之后渲染成什么组件, 比如上面的代码会被渲染成一个<li>元素, 而不是<a>**

**3.replace: replace不会留下history记录, 所以指定replace的情况下, 后退键返回不能返回到上一个页面中** 

/*就是说router-link使用的是pushState方法 但是 我们给加上replace这个方法后，浏览器就不会在让我们使用左上角的回到上一个页面的按钮了*/

**4.active-class: 当<router-link>对应的路由匹配成功时, 会自动给当前元素设置一个router-link-active的class, 设置active-class可以修改默认的名称.**

在进行高亮显示的导航菜单或者底部tabbar时, 会使用到该类.

但是通常不会修改类的属性, 会直接使用默认的router-link-active即可. 

![image-20210509174617737](./media/image-20210509174617737.png)



但是使用这种方法的话 我们要是多个类名 或者多个 router-link呢 这个时候我们就可以在

**5.可以通过路由的构造选项 linkActiveClass 来全局配置，不用在每个 <router-link> 使用 active-class 指定生成的类名**   

该class具体的名称也可以通过router实例的属性进行修改 

![image-20210509175018719](./media/image-20210509175018719.png)

 

#### 声明式与编程式路由

【也就是通过代码跳转路由】

| **声明式**( **直接通过** **<a>** 标签href指定链接跳转) | 编程式(采用 js 代码链接跳转,如localhost.href) |
| ------------------------------------------------------ | --------------------------------------------- |
| <router-link :to="...">                                | router.push(...)                              |

编程式路由导航API

![image-20210509180514789](./media/image-20210509180514789.png)

 通过代码的方式修改路由 vue-router

   .push => pushState  pushState代表着可以返回

  .replace=>replaceState  代表着替换可以替换不可以返回

有时候, 页面的跳转可能需要执行对应的JavaScript代码, 这个时候, 就可以使用第二种跳转方式了

![image-20210509183144782](./media/image-20210509183144782.png)

#### $route 和 $router之间的区别

【B站收藏vue入门到精通 114 具体讲解】

$route是哪一个路由处于活跃状态就拿到哪一个路由的信息 【就是说我们点击到这个路由就可以拿到里面的信息】

$router是我们整个大的路由  const router = new VueRouter({}）的对象

**$route和$router是有区别的**
$router为VueRouter实例，想要导航到不同URL，则使用$router.push方法
$route为当前router跳转对象里面可以获取name、path、query、params等 

**所有的组件都继承自VUE的原型**

#### 拿到VUE-router 和 vuex的时候为什么要use

因为执行vue.use的时候 会执行里面的install这个内容

# 动态路由

###### 【1】

在某些情况下，一个页面的path路径可能是不确定的，比如我们进入用户界面时，希望是如下的路径：
/user/aaaa或/user/bbbb
除了有前面的/user之外，后面还跟上了用户的ID
这种path和Component的匹配关系，我们称之为动态路由(也是路由传递数据的一种方式)。params：参数

![image-20210509200954604](./media/image-20210509200954604.png)

![image-20210509200959704](./media/image-20210509200959704.png)

![image-20210509201006038](./media/image-20210509201006038.png)

输出结果：

![image-20210509201011829](./media/image-20210509201011829.png)

通过$route.params.id拿到id 渲染到页面中

###### 【2】

再路由组件中index.js path后面添加：id【这个自己起的  也可以叫useid】

![image-20210509201345618](./media/image-20210509201345618.png)

APP.Vue 在router-link中动态绑定：to 可以拿到data中的数据

![image-20210509201736536](./media/image-20210509201736536.png)

![image-20210509201756663](./media/image-20210509201756663.png)

输出结果

![image-20210509201851859](./media/image-20210509201851859.png)



#### 对于vue-router打包文件的解析

正常webpack打包完出现下面的文件格式 

![image-20210510145929466](./media/image-20210510145929466.png)

app放我们自己写的代码

main放对一些底层的东西 比如 有些浏览器 不支持commonJS和ES6所以 我们这个里面 放一些支持代码，或者是各个模块相互依赖的东西

vendor 放第三方相关的东西 比如 vue vue-router axios等等 第三方的框架或者其他

#### vue-router路由懒加载

**官方给出了解释:**
当打包构建应用时，Javascript 包会变得非常大，影响页面加载。

如果我们能把不同路由对应的组件分割成不同的代码块，然后当路由被访问的时候才加载对应组件，这样就更加高效了
**官方在说什么呢?**
首先, 我们知道路由中通常会定义很多不同的页面.

这个页面最后被打包在哪里呢? 一般情况下, 是放在一个js文件中.

但是, 页面这么多放在一个js文件中, 必然会造成这个页面非常的大.

如果我们一次性从服务器请求下来这个页面, 可能需要花费一定的时间, 甚至用户的电脑上还出现了短暂空白的情况.

如何避免这种情况呢? 使用路由懒加载就可以了.
**路由懒加载做了什么?** **嘎嘎节省空间**
路由懒加载的主要作用就是将路由对应的组件打包成一个个的js代码块.

只有在这个路由被访问到的时候, 才加载对应的组件

###### 路由懒加载效果

没有懒加载之前的效果打包出三个js文件

![image-20210510165110857](./media/image-20210510165110857.png)

懒加载之后打包的效果

![image-20210510165211503](./media/image-20210510165211503.png)

###### 懒加载的方式

方式一: 结合Vue的异步组件和Webpack的代码分析.【最早期的路由懒加载方式】

```javascript
const Home = resolve => { require.ensure(['../components/Home.vue'], () => { resolve(require('../components/Home.vue')) })};
```

方式二: AMD写法【不咋用】

```javascript
const About = resolve => require(['../components/About.vue'], resolve);
```

方式三: 在ES6中, 我们可以有更加简单的写法来组织Vue异步组件和Webpack的代码分割. 【推荐这种方法使用】

```javascript
const Home = () => import('../components/Home.vue')
```

#### 路由的嵌套

##### 1.什么是嵌套路由

嵌套路由是一个很常见的功能

比如在home页面中, 我们希望通过/home/news和/home/message访问一些内容.

一个路径映射一个组件, 访问这两个路径也会分别渲染两个组件.

路径和组件的关系如下:

![image-20210510174926431](./media/image-20210510174926431.png)

要想实现嵌套路由必须实现以下两个步骤

1.创建对应的子组件，并且在 路由映射中配置自己的子路由

2.在组件内部中使用<router-view>标签

##### 2.嵌套路由的实现

###### 1.先注册子组件

创建homeMessage.vue组件 和 homeNews.vue组件

![image-20210510175325020](./media/image-20210510175325020.png)

###### 2.配置router子组件

在router文件夹下面的index.js中配置文件 因为是Home组件中的子路由 所以我们在home组件中配置  

【子路由里面不需要/】

![image-20210510175552483](./media/image-20210510175552483.png)

###### 3.在组件内部中使用<router-view>标签

因为组件是home组件的子组件所以我们想要news和message在home组件中显示出来，在home中添加<router-view> 和router-link标签 

注意 to指向的位置一定要加上/

![image-20210510175856673](./media/image-20210510175856673.png)

###### 4.解决嵌套默认路径的问题

设置网页重定向指向news

```javascript
children: [
       {
         path: '',
         redirect: 'news'
       },
      ]
```

#### VUE-router传递参数

 先前我们拿到数据通过$route.params的方式拿到了一次数据

##### 传递参数的方式

**传递参数主要有两种类型: params和query**

**params的类型:**

配置路由格式: /router/:id

传递的方式: 在path后面跟上对应的值

传递后形成的路径: /router/123, /router/abc

**query的类型:** 【必须是对象的模式】

配置路由格式: /router, 也就是普通配置

传递的方式: 对象中使用query的key作为传递方式

传递后形成的路径: /router?id=123, /router?id=abc

如何使用它们呢? 也有两种方式: <router-link>的方式和JavaScript代码方式

###### 列子

为了演示传递参数, 我们这里再创建一个组件, 并且将其配置好
第一步: 创建新的组件Profile.vue 

![image-20210512200028234](./media/image-20210512200028234.png)

第二步: 配置路由映射 

![image-20210512200039133](./media/image-20210512200039133.png)

第三步: 添加跳转的<router-link>

![image-20210512200046750](./media/image-20210512200046750.png) 

第四步：router-link  to动态绑定 一个对象 里面有path和我们要传递的query【注意：query的格式也是一个对象】

![image-20210512200201066](./media/image-20210512200201066.png)

###### 传递参数的第二种方法

传递参数方式二: JavaScript代码

第一步：定义一个按钮绑定一个方法

![image-20210512200452505](./media/image-20210512200452505.png)

第二步：在methods里面给我们要用的方法定义出来

![image-20210512200520252](./media/image-20210512200520252.png)

第三步：获取参数

获取参数通过$route对象获取的.
在使用了 vue-router 的应用中，路由对象会被注入每个组件中，赋值为 this.$route ，并且当路由切换时，路由对象会被更新。
通过$route获取传递的信息如下:

在profile组件中获取传递的信息

![image-20210512200643154](./media/image-20210512200643154.png)

输出结果  【既获取到query也获取到了 我们想要传递的数据】

![image-20210512200733837](./media/image-20210512200733837.png)

# 导航守卫

### [前置守卫(guard) ]

官网学习：https://router.vuejs.org/zh/guide/advanced/navigation-guards.html#%E8%B7%AF%E7%94%B1%E7%8B%AC%E4%BA%AB%E7%9A%84%E5%AE%88%E5%8D%AB

#### 为什么使用导航守卫?

**我们来考虑一个需求: 在一个SPA应用中, 如何改变网页的标题呢?**
网页标题是通过<title>来显示的, 但是SPA只有一个固定的HTML, 切换不同的页面时, 标题并不会改变.
但是我们可以通过JavaScript来修改<title>的内容.window.document.title = '新的标题'.

**那么在Vue项目中, 在哪里修改? 什么时候修改比较合适呢?**

**普通的修改方式:**
我们比较容易想到的修改标题的位置是每一个路由对应的组件.vue文件中.
通过mounted声明周期函数, 执行对应的代码进行修改即可.
但是当页面比较多时, 这种方式不容易维护(因为需要在多个页面执行类似的代码).
有没有更好的办法呢? 使用导航守卫即可.
**什么是导航守卫?**
vue-router提供的导航守卫主要用来监听监听路由的进入和离开的.
vue-router提供了beforeEach和afterEach的钩子函数, 它们会在路由即将改变前和改变后触发.

#### 导航守卫的使用

我们可以利用beforeEach来完成标题的修改.  beforeEach是一个函数，他的里面还需要传入三个参数（to,from,next）
首先, 我们可以在钩子当中定义一些标题, 可以利用meta来定义
其次, 利用导航守卫,修改我们的标题.

![image-20210516111200279](./media/image-20210516111200279.png)



导航钩子的三个参数解析:
to: 即将要进入的目标的路由对象.
from: 当前导航即将要离开的路由对象.
next: 调用该方法后, 才能进入下一个钩子（目标路由）

在路由守卫中必须调用next（）方法     next（）默认路径

![image-20210516111238297](./media/image-20210516111238297.png)

###### 为什么要用matched

因为我们不写上matched的话 我们第一个首页路由里面是嵌套路由，就没有办法锁定meta里面的title了

**我们在前置守卫beforeEach钩子函数中输出to**

![image-20210516112604955](./media/image-20210516112604955.png)

meta中找不到title 但是在matche[0]中可以找到meta.title 所以我们就可以找到 首页的meta下面的title了

**没加matche[0]效果图**undefined

![image-20210516112834505](./media/image-20210516112834505.png)

**加上matche[0]效果图**

![image-20210516112923069](./media/image-20210516112923069.png)

#### 导航守卫的理解

当一个导航触发时，全局前置守卫按照创建顺序调用。守卫是异步解析执行，此时导航在所有守卫 resolve 完之前一直处于 **等待中**。

每个守卫方法接收三个参数：

- **`to: Route`**: 即将要进入的目标 [路由对象](https://router.vuejs.org/zh/api/#路由对象)
- **`from: Route`**: 当前导航正要离开的路由

- **`next: Function`**: 一定要调用该方法来 **resolve** 这个钩子。执行效果依赖 `next` 方法的调用参数。
  - **`next()`**: 进行管道中的下一个钩子。如果全部钩子执行完了，则导航的状态就是 **confirmed** (确认的)。
  - **`next(false)`**: 中断当前的导航。如果浏览器的 URL 改变了 (可能是用户手动或者浏览器后退按钮)，那么 URL 地址会重置到 `from` 路由对应的地址。
  - **`next('/')` 或者 `next({ path: '/' })`**: 跳转到一个不同的地址。当前的导航被中断，然后进行一个新的导航。你可以向 `next` 传递任意位置对象，且允许设置诸如 `replace: true`、`name: 'home'` 之类的选项以及任何用在 [`router-link` 的 `to` prop](https://router.vuejs.org/zh/api/#to) 或 [`router.push`](https://router.vuejs.org/zh/api/#router-push) 中的选项。
  - **`next(error)`**: (2.4.0+) 如果传入 `next` 的参数是一个 `Error` 实例，则导航会被终止且该错误会被传递给 [`router.onError()`](https://router.vuejs.org/zh/api/#router-onerror) 注册过的回调。

**确保 `next` 函数在任何给定的导航守卫中都被严格调用一次。它可以出现多于一次，但是只能在所有的逻辑路径都不重叠的情况下，否则钩子永远都不会被解析或报错**。这里有一个在用户未能验证身份时重定向到 `/login` 的示例：

```js
// BAD
router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' })
  // 如果用户未能验证身份，则 `next` 会被调用两次
  next()
})
// GOOD
router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' })
  else next()
})
```

#### 后置钩子(afterEach)  钩子英文：hook

补充一:如果是后置钩子, 也就是afterEach, 不需要主动调用next()函数.

你也可以注册全局后置钩子，然而和守卫不同的是，这些钩子不会接受 `next` 函数也不会改变导航本身：

```js
router.afterEach((to, from) => {
  // ...
})
```

补充二: 上面我们使用的导航守卫, 被称之为全局守卫.

###### **路由独享的守卫.**

只有我进入路由里面，才会进行的回调函数

你可以在路由配置上直接定义 `beforeEnter` 守卫：

```js
const router = new VueRouter({
  routes: [
    {
      path: '/foo',
      component: Foo,
      beforeEnter: (to, from, next) => {
        // ...
      }
    }
  ]
})
```

这些守卫与全局前置守卫的方法参数是一样的。

**路由独享守卫练习**

在index.js的about里面添加我们想要的路由独享守卫  

 **进入到 about的时候 才输出我们在下面 设置的前置后卫和后置钩子中的 ++++ -----**

![image-20210516115328467](./media/image-20210516115328467.png)

![image-20210516115032545](./media/image-20210516115032545.png)

输出

![image-20210516115133797](./media/image-20210516115133797.png)

###### 组件内的守卫

最后，你可以在路由组件内直接定义以下路由导航守卫：

- `beforeRouteEnter`
- `beforeRouteUpdate` (2.2 新增)
- `beforeRouteLeave`

```js
const Foo = {
  template: `...`,
  beforeRouteEnter(to, from, next) {
    // 在渲染该组件的对应路由被 confirm 前调用
    // 不！能！获取组件实例 `this`
    // 因为当守卫执行前，组件实例还没被创建
  },
  beforeRouteUpdate(to, from, next) {
    // 在当前路由改变，但是该组件被复用时调用
    // 举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的时候，
    // 由于会渲染同样的 Foo 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调用。
    // 可以访问组件实例 `this`
  },
  beforeRouteLeave(to, from, next) {
    // 导航离开该组件的对应路由时调用
    // 可以访问组件实例 `this`
  }
}
```

`beforeRouteEnter` 守卫 **不能** 访问 `this`，因为守卫在导航确认前被调用，因此即将登场的新组件还没被创建。

不过，你可以通过传一个回调给 `next`来访问组件实例。在导航被确认的时候执行回调，并且把组件实例作为回调方法的参数。

```js
beforeRouteEnter (to, from, next) {
  next(vm => {
    // 通过 `vm` 访问组件实例
  })
}
```

注意 `beforeRouteEnter` 是支持给 `next` 传递回调的唯一守卫。对于 `beforeRouteUpdate` 和 `beforeRouteLeave` 来说，`this` 已经可用了，所以**不支持**传递回调，因为没有必要了。

```js
beforeRouteUpdate (to, from, next) {
  // just use `this`
  this.name = to.params.name
  next()
}
```

这个离开守卫通常用来禁止用户在还未保存修改前突然离开。该导航可以通过 `next(false)` 来取消。

```js
beforeRouteLeave (to, from, next) {
  const answer = window.confirm('Do you really want to leave? you have unsaved changes!')
  if (answer) {
    next()
  } else {
    next(false)
  }
}
```

### keep-alive遇见vue-router

keep-alive 是 Vue 内置的一个组件，可以使被包含的组件保留状态，或避免重新渲染。
它们有两个非常重要的属性:

include - 字符串或正则表达，只有匹配的组件会被缓存
exclude - 字符串或正则表达式，任何匹配的组件都不会被缓存

router-view 也是一个组件，如果直接被包在 keep-alive 里面，所有路径匹配到的视图组件都会被缓存：

![image-20210516173328970](./media/image-20210516173328970.png)

通过create声明周期函数来验证

#### 通过keep-live我‘们学到两个函数

```js
 // 这两个函数, 只有该组件被保持了状态使用了keep-alive时, 才是有效的

    /* 当界面路由进入活跃状态后执行这个函数 */
    activated() {
      this.$router.push(this.path);
      console.log('activated');
    },
    /* 当界面路由不进入活跃状态后执行这个函数 */
    deactivated() {
      console.log('deactivated');
    },
```

大概什么意思呢 就是 当我们keep-live包括了我们想要的router-view的时候，这两个函数是可以生效的

在APPvue之中

要想在keep-alive之中不想一直被缓存的情况下，我们可以使用exclude这个方法【这里面千万不可以加上空格只能用，分割】

include也是用name这种方式 

在我们的 profile和user组件中起一个name的名字

![image-20210516173742582](./media/image-20210516173742582.png)

User.vue

![image-20210516174835649](./media/image-20210516174835649.png)

Profile.vue之中

![image-20210516174916350](./media/image-20210516174916350.png)

在homevue之中

![image-20210516173811034](./media/image-20210516173811034.png)

输出结果

![image-20210516180305619](./media/image-20210516180305619.png)

先点击用户里面没有绑定keepalive 所以不打印activated，后面点击首页 打印出activated

## SVG图片：是通过svg标签画出来的 里面的内容不允许更改

# MVC和MVVM之间的区别

![image-20210612193207952](./media/image-20210612193207952.png)

# VueX

## vueX是干什么的？

**官方解释**：Vuex 是一个专为 Vue.js 应用程序开发的状态管理模式。
它采用 集中式存储管理 应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。
Vuex 也集成到 Vue 的官方调试工具 devtools extension，提供了诸如零配置的 time-travel 调试、状态快照导入导出等高级调试功能。
**状态管理到底是什么？**
状态管理模式、集中式存储管理这些名词听起来就非常高大上，让人捉摸不透。
其实，你可以简单的将其看成把需要多个组件共享的变量全部存储在一个对象里面。
然后，将这个对象放在顶层的Vue实例中，让其他组件可以使用。
那么，多个组件是不是就可以共享这个对象中的所有变量属性了呢？
等等，如果是这样的话，为什么官方还要专门出一个插件Vuex呢？难道我们不能自己封装一个对象来管理吗？
当然可以，只是我们要先想想VueJS带给我们最大的便利是什么呢？没错，就是响应式。
如果你自己封装实现一个对象能不能保证它里面所有的属性做到响应式呢？当然也可以，只是自己封装可能稍微麻烦一些。
不用怀疑，Vuex就是为了提供这样一个在多个组件间共享状态的插件，用它就可以了。

## 管理什么状态呢

但是，有什么状态时需要我们在多个组件间共享的呢？
如果你做过大型开放，你一定遇到过多个状态，在多个界面间的共享问题。
比如用户的登录状态token：令牌、用户名称、头像、地理位置信息等等。
比如商品的收藏、购物车中的物品等等。
这些状态信息，我们都可以放在统一的地方，对它进行保存和管理，而且它们还是响应式的（待会儿我们就可以看到代码了，莫着急）。

## 单界面的状态管理

我们知道，要在单个组件中进行状态管理是一件非常简单的事情
什么意思呢？我们来看下面的图片。
这图片中的三种东西，怎么理解呢？
State：不用多说，就是我们的状态。（你姑且可以当做就是data中的属性）
View：视图层，可以针对State的变化，显示不同的信息。（这个好理解吧？）
Actions：这里的Actions主要是用户的各种操作：点击、输入等等，会导致状态的改变。

![image-20210704173047223](./media/image-20210704173047223.png)

## 单界面状态管理的实现

![image-20210704173111285](./media/image-20210704173111285.png)

在这个案例中，我们有木有状态需要管理呢？没错，就是个数counter。
counter需要某种方式被记录下来，也就是我们的State。
counter目前的值需要被显示在界面中，也就是我们的View部分。
界面发生某些操作时（我们这里是用户的点击，也可以是用户的input），需要去更新状态，也就是我们的Actions
这不就是上面的流程图了吗？

## 多界面状态管理

Vue已经帮我们做好了单个界面的状态管理，但是如果是多个界面呢？
多个试图都依赖同一个状态（一个状态改了，多个界面需要进行更新）
不同界面的Actions都想修改同一个状态（Home.vue需要修改，Profile.vue也需要修改这个状态）
也就是说对于某些状态(状态1/状态2/状态3)来说只属于我们某一个试图，但是也有一些状态(状态a/状态b/状态c)属于多个试图共同想要维护的
状态1/状态2/状态3你放在自己的房间中，你自己管理自己用，没问题。
但是状态a/状态b/状态c我们希望交给一个大管家来统一帮助我们管理！！！
没错，Vuex就是为我们提供这个大管家的工具。
全局单例模式（大管家）
我们现在要做的就是将共享的状态抽取出来，交给我们的大管家，统一进行管理。
之后，你们每个试图，按照我规定好的规定，进行访问和修改等操作。
这就是Vuex背后的基本思想。

## Vuex状态管理图例

![image-20210704173240255](./media/image-20210704173240255.png)

​	devtools主要用来追踪状态管理的

## 简单的案例 VUEX

我们还是实现一下之前简单的案例
![image-20210704181328538](./media/image-20210704181328538.png)

首先，我们需要在某个地方存放我们的Vuex代码：
这里，我们先创建一个文件夹store，并且在其中创建一个index.js文件
在index.js文件中写入如下代码：

```js
import Vue from 'vue'
import Vuex from 'vuex'
// 1.安装插件  会自动执行里面的install方法
Vue.use(Vuex)
//2.创建对象
const store = new Vuex.Store({
state:{
    counter:1000
},
// 定义一些方法
mutations:{
    //默认有一个参数state  对应上面的state对象
  increment(state){
     state.counter++
  },
  decrement(state){
      state.counter--
  }
},
actions:{

},
getters:{

},
modules:{

}
})

//3.导出store对象
export default store
```

### 挂载到Vue实例中

次，我们让所有的Vue组件都可以使用这个store对象
来到**main.js**文件，导入store对象，并且放在new Vue中
这样，在其他Vue组件中，我们就可以通过this.$store的方式，获取到这个store对象了

```js
import Vue from 'vue'
import App from './App.vue'
import store from './store'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')

```

### 使用Vuex的count

好的，这就是使用Vuex最简单的方式了。
我们来对使用步骤，做一个简单的小节：
1.提取出一个公共的store对象，用于保存在多个组件中共享的状态
2.将store对象放置在new Vue对象中，这样可以保证在所有的组件中都可以使用到
3.在其他组件中使用store对象中保存的状态即可
通过this.$store.state.属性的方式来访问状态
**通过this.$store.commit('mutation中方法')来修改状态**
注意事项：
我们通过提交mutation的方式，而非直接改变store.state.count。
这是因为Vuex可以更明确的追踪状态的变化，所以不要直接改变store.state.count的值。

```js
<template>
 <div id="app">
   <h1>-----APP内容-----</h1>
  <h2>{{message}}</h2>
  <h2>{{$store.state.counter}}</h2>
  <button @click="addition">+1</button>
  <button @click="subtraction">-1</button>

<h1>-------helloworld内容------</h1>
  <hello/>
 </div>
</template>

<script>
import Hello from './components/HelloWorld.vue'
export default {
  name:"App",
 data () {
 return {
   message:'我是APP组件',
   counter:0
 }
 },

 components: {
   Hello
 },

 methods: {
  addition(){
this.$store.commit('increment')
  },
  subtraction(){
this.$store.commit('decrement')
  }
 }
}
</script>

<style scoped>
</style>

```

## devtools追踪mutations

![image-20210704181738029](./media/image-20210704181738029.png)

点击会获取到mutation中的状态 定义的函数方法

## Vuex核心概念

Vuex有几个比较核心的概念:
1.State 单一状态树
2.Getters 相当于computed
3.Mutation  处理各种方法【函数】的
4Action   这个处理异步的操作 再通过mutation操作
5.Module 划分不同的模块 
我们对它进行一一介绍.

# State单一状态树

Vuex提出使用单一状态树, 什么是单一状态树呢？
英文名称是Single Source of Truth，也可以翻译成单一数据源。
但是，它是什么呢？我们来看一个生活中的例子。OK，我用一个生活中的例子做一个简单的类比。

我们知道，在国内我们有很多的信息需要被记录，比如上学时的个人档案，工作后的社保记录，公积金记录，结婚后的婚姻信息，以及其他相关的户口、医疗、文凭、房产记录等等（还有很多信息）。
这些信息被分散在很多地方进行管理，有一天你需要办某个业务时(比如入户某个城市)，你会发现你需要到各个对应的工作地点去打印、盖章各种资料信息，最后到一个地方提交证明你的信息无误。
这种保存信息的方案，不仅仅低效，而且不方便管理，以及日后的维护也是一个庞大的工作(需要大量的各个部门的人力来维护，当然国家目前已经在完善我们的这个系统了)。
这个和我们在应用开发中比较类似：
如果你的状态信息是保存到多个Store对象中的，那么之后的管理和维护等等都会变得特别困难。
所以Vuex也使用了单一状态树来管理应用层级的全部状态。【说白了就是全放在一个store里面】
单一状态树能够让我们最直接的方式找到某个状态的片段，而且在之后的维护和调试过程中，也可以非常方便的管理和维护。

## Getters基本使用

### 案例一

有时候，我们需要从store中获取一些state变化后的状态，比如下面的Store中： 类似计算属性

获取state里面counter的平方

1.可以在store里面定义getters 方法 powerCounter

![image-20210704204842344](./media/image-20210704204842344.png)

2.在APP.vue里面显示出来

![image-20210704204848308](./media/image-20210704204848308.png)

3.输出结果

![image-20210704204915006](./media/image-20210704204915006.png)

### 案例二

1.可以在store里面定义getters 方法 more20stu

![image-20210704205819785](./media/image-20210704205819785.png)

2.在APP.vue，helloworld.vue里面显示出来

APP

![image-20210704205825418](./media/image-20210704205825418.png)

Helloworld

![image-20210704205845639](./media/image-20210704205845639.png)

3.输出结果

![image-20210704205850729](./media/image-20210704205850729.png)

## getters代码传参问题

需求 获取所有超过二十岁的人数【不要个人信息 只要人数】

1.getters传值

![image-20210704210658139](./media/image-20210704210658139.png)

2.APP.vue显示

![image-20210704210703761](./media/image-20210704210703761.png)

3.输出结果

![image-20210704210708568](./media/image-20210704210708568.png)

### 案例二

需求 ：如果过滤出比我们在APP.vue里面传入的age还小的对象呢？换句话说 app.vue里面传入的age如何让vueX里面getter获取到呢？

1.在getter里面设置方法 moreAgeStu 返回成一个函数样子 这样再APP.vue里面就可以传入参数age了

![image-20210704211947209](./media/image-20210704211947209.png)

2.APP.VUE输出

![image-20210704212003118](./media/image-20210704212003118.png)

3.输出结果

![image-20210704212017656](./media/image-20210704212017656.png)

## Mutation状态更新

Vuex的store状态的更新唯一方式：提交Mutation 【更改state必须通过mutation】
Mutation主要包括两部分：
字符串的事件类型（type）
一个回调函数（handler）,该回调函数的第一个参数就是state。
mutation的定义方式：

![image-20210704212522828](./media/image-20210704212522828.png)

通过mutation更新

![image-20210704212616395](./media/image-20210704212616395.png)

![image-20210704212620092](./media/image-20210704212620092.png)

## Mutation传递参数

参数被称为是mutation的载荷(Payload)

但是如果参数不是一个呢?
比如我们有很多参数需要传递.
这个时候, 我们通常会以对象的形式传递, 也就是payload是一个对象.
这个时候可以再从对象中取出相关的信息.

【下面的例子全面讲解】

1.在APP.vue里面注册方法 并且传递过去我们要的参数

![image-20210704214959600](./media/image-20210704214959600.png)

![image-20210704215009678](./media/image-20210704215009678.png)

2.store/index.js里面接受参数并且添加各种处理方法VUEX

![image-20210704215023272](./media/image-20210704215023272.png)

3.‘输出结果

![image-20210704215035082](./media/image-20210704215035082.png)

### Mutation提交风格

上面的通过commit进行提交是一种普通的方式

**Vue还提供了另外一种风格, 它是一个包含type属性的对象**

![image-20210704220838450](./media/image-20210704220838450.png)

**Mutation中的处理方式是将整个commit的对象作为payload使用, 所以代码没有改变, 依然如下:**

![image-20210704220846930](./media/image-20210704220846930.png)

输出结果

![image-20210704220909342](./media/image-20210704220909342.png)

### Mutation相应规则

Vuex的store中的state是响应式的, 当state中的数据发生改变时, Vue组件会自动更新.

**提前在store中初始化好所需的属性. 后加进去的属性不会加入到响应式内容里面**

下面是一个案例

1.在store/index.js/state里面添加info

![image-20210705122557010](./media/image-20210705122557010.png)

2.在app.vue里面配置对应的方法和输出

![image-20210705123106451](./media/image-20210705123106451.png)

3.输出结果

![image-20210705123130781](./media/image-20210705123130781.png)

4.更改了state里面的数据后 也会改变

![image-20210705123258877](./media/image-20210705123258877.png)

![image-20210705123303571](./media/image-20210705123303571.png)

5.但是我们向要往info数据里面添加数据呢会不会是响应式？

在template里面添加方法 updateInfo 在methdos里面注册

![image-20210705123845032](./media/image-20210705123845032.png)

5.（1）在index.js里面注册方法

![image-20210705123917509](./media/image-20210705123917509.png)

5.（2）输出结果

![image-20210705123932801](./media/image-20210705123932801.png)

可以看到在 devtools里面输出的是可以看到有数据的 但是页面显示却不显示出来 代表了数据不是响应式的 也就验证了前面那句话

**提前在store中初始化好所需的属性. 后加进去的属性不会加入到响应式内容里面**

但是我们就是想后来加入数据 变成响应式的呢应该用那种方法呢？

1. ```js
   Vue.set(state.info, 'address', '洛杉矶') //向state里面info添加地址数据变成响应式
   //里面三个参数 第一个是加入state里面那个对象名称，
   //第二个是 添加的key名称、【也可以是一个number类型 就是代表着数组的下标】 
   //第三个是value 
   ```

   ```js
   Vue.delete(state.info, 'age')//向state里面info删除数据并且变成响应式
   ```

   6.最终结果

![image-20210705124449513](./media/image-20210705124449513.png)

## Action的基本定义

**mutation里面可以执行异步操作但是 devtools不会追踪，不会跟进 所以强烈不建议mutations进行异步操作**

我们强调, 不要再Mutation中进行异步操作.
但是某些情况, 我们确实希望在Vuex中进行一些异步操作, 比如网络请求, 必然是异步的. 这个时候怎么处理呢?
Action类似于Mutation, 但是是用来代替Mutation进行异步操作的.

Action的基本使用代码如下:

![image-20210705133201337](./media/image-20210705133201337.png)context是什么?
context是和store对象具有相同方法和属性的对象.
也就是说, 我们可以通过context去进行commit相关的操作, 也可以获取context.state等.
但是注意, 这里它们并不是同一个对象, 为什么呢? 我们后面学习Modules的时候, 再具体说.
这样的代码是否多此一举呢?
我们定义了actions, 然后又在actions中去进行commit, 这不是脱裤放屁吗?
事实上并不是这样, 如果在Vuex中有异步操作, 那么我们就可以在actions中完成了.

**在Vue组件中, 如果我们调用action中的方法, 那么就需要使用dispatch**

![image-20210705133253741](./media/image-20210705133253741.png)

**同样的, 也是支持传递payload**

![image-20210705133302301](./media/image-20210705133302301.png)

## Action返回的Promise

前面我们学习ES6语法的时候说过, Promise经常用于异步操作.
在Action中, 我们可以将异步操作放在一个Promise中, 并且在成功或者失败后, 调用对应的resolve或reject.
OK, 我们来看下面的代码:

![image-20210705133347167](./media/image-20210705133347167.png)

![image-20210705133355453](./media/image-20210705133355453.png)

### 项目结构

![image-20210705134956408](./media/image-20210705134956408.png)

